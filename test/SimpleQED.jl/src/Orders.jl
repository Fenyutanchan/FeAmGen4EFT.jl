# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

QED = CouplingOrder("QED")

all_coupling_orders = Dict(
    "QED" => QED
)
