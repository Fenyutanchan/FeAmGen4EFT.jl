# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

EM_electron = Vertex(
    "EM_electron",
    [electron, anti_electron, photon],
    [
        VertexExpression(
            FFV,
            Dict{Group, String}(),
            "1"
        )
    ],
    EM
)

EM_muon = Vertex(
    "EM_muon",
    [muon, anti_muon, photon],
    [
        VertexExpression(
            FFV,
            Dict{Group, String}(),
            "1"
        )
    ],
    EM
)

all_vertices = Dict(
    "EM_electron" => EM_electron,
    "EM_muon" => EM_muon
)
