# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

EM = Coupling(
    "EM",
    "-im * ee",
    Dict(
        QED => 1
    ),
)

all_couplings = Dict(
    "EM" => EM
)
