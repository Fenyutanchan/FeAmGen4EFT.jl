# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

electron = Particle(
    "electron",
    11,
    2,
    Representation[],
    me,
    ZERO,
    Dict{String, Real}("Q" => -1)
)
anti_electron = anti(electron)

muon = Particle(
    "muon",
    13,
    2,
    Representation[],
    mμ,
    ZERO,
    Dict{String, Real}("Q" => -1)
)
anti_muon = anti(muon)

photon = Particle(
    "photon",
    22,
    3,
    Representation[],
    ZERO,
    ZERO,
    Dict{String, Real}();
    self_conjugate_flag=true,
    gauge_boson_flag=true
)

all_particles = Dict(
    "electron" => electron,
    "anti_electron" => anti_electron,
    "muon" => muon,
    "anti_muon" => anti_muon,
    "photon" => photon
)
