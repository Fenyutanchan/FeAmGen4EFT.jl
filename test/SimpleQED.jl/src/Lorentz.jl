# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

FFV = LorentzStructure(
    "FFV",
    [2, 2, 3],
    "Gamma(3,2,1)"
)
