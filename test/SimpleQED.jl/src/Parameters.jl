# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

ZERO = RealInternalParameter("ZERO", 0)
ee = ExternalParameter("ee", 0.3)
me = ExternalParameter("me", 511) # in eV
mμ = ExternalParameter("mmu", 106e6) # in eV

all_parameters = Dict(
    "ZERO" => ZERO,
    "ee" => ee,
    "me" => me,
    "mμ" => mμ
)
