# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

module SimpleQED

using UniversalFeynmanModel

include("Parameters.jl")

include("Lorentz.jl")

include("Groups.jl")

include("Representations.jl")

include("GroupFunctions.jl")

include("Orders.jl")

include("Couplings.jl")

include("Particles.jl")

include("Vertices.jl")

end # module SimpleQED
