# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

import SimpleQCD

using FeAmGen4EFT

FeAmGen4EFT.run_Qgraf(
    SimpleQCD,
    1,
    ["quark", "anti_quark"],
    ["quark", "anti_quark"],
    ["notadpole", "onshell"];
    CT_order_list=Dict("QCDCT" => 1)
)

all_Feynman_diagrams = FeAmGen4EFT.analyze_Qgraf_output(
    SimpleQCD,
    joinpath(@__DIR__, "qgraf_output.out")
)

# one_Feynman_diagram = first(all_Feynman_diagrams)
# one_vertex = first(one_Feynman_diagram.internal_vertex_list)
# @show FeAmGen4EFT.analyze_vertex_structure(SimpleQCD, one_Feynman_diagram, one_vertex)

# one_propagator = first(one_Feynman_diagram.propagator_list)
# one_external_leg = first(one_Feynman_diagram.external_leg_list)
# @show FeAmGen4EFT.analyze_propagator_structure(one_Feynman_diagram, one_propagator)
# @show FeAmGen4EFT.analyze_external_leg_structure(one_Feynman_diagram, one_external_leg)

for (diagram_index, one_Feynman_diagram) ∈ enumerate(all_Feynman_diagrams)
    println("The amplitude for Feynman diagram $diagram_index is:")
    @show FeAmGen4EFT.generate_amplitude(SimpleQCD, one_Feynman_diagram)
end

rm("Feynman_diagrams.log"; force=true, recursive=true)
open("Feynman_diagrams.log", "w+") do io
    for Feynman_diagram ∈ all_Feynman_diagrams
        write(io, "$Feynman_diagram\n")
    end
end
