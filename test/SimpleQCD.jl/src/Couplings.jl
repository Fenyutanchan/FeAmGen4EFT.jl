# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

ig = Coupling(
    "ig",
    "im * gS",
    Dict(
        QCD => 1
    )
)

g = Coupling(
    "g",
    "gS",
    Dict(
        QCD => 1
    )
)

ig_sqr = Coupling(
    "ig_sqr",
    "im * gS^2",
    Dict(
        QCD => 2
    )
)

ig_Z2_Z3sqrt = Coupling(
    "ig_Z2_Z3sqrt",
    "im * gS * (1 + dZ2) * (1 + dZ3)^(1/2)",
    Dict(
        QCD => 1,
        QCDCT => 1
    )
)

g_Z33over2 = Coupling(
    "g_Z33over2",
    "gS * (1 + dZ3)^(3/2)",
    Dict(
        QCD => 1,
        QCDCT => 1
    )
)

ig_sqr_Z3sqr = Coupling(
    "ig_sqr_Z3sqr",
    "im * gS^2 * (1 + dZ3)^2",
    Dict(
        QCD => 2,
        QCDCT => 1
    )
)

g_Zghost_Z3sqrt = Coupling(
    "g_Zghost_Z3sqrt",
    "gS * (1 + dZghost) * (1 + dZ3)^(1/2)",
    Dict(
        QCD => 1,
        QCDCT => 1
    )
)

iZ3 = Coupling(
    "iZ3",
    "im * (1 + dZ3)",
    Dict(
        QCDCT => 1
    )
)

pure_im = Coupling(
    "pure_im",
    "im",
    Dict(
        QCDCT => 1
    )
)

iZghost = Coupling(
    "Zghost",
    "im * (1 + dZghost)",
    Dict(
        QCDCT => 1
    )
)

all_couplings = Dict(
    "ig" => ig,
    "g" => g,
    "ig_sqr" => ig_sqr,

    "ig_Z2_Z3sqrt" => ig_Z2_Z3sqrt,
    "g_Z33over2" => g_Z33over2,
    "ig_sqr_Z3sqr" => ig_sqr_Z3sqr,
    "g_Zghost_Z3sqrt" => g_Zghost_Z3sqrt,

    "iZ3" => iZ3,
    "pure_im" => pure_im,
    "iZghost" => iZghost
)
