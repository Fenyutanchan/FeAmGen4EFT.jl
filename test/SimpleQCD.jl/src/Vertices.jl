# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

fermion_vertex = Vertex(
    "fermion_vertex",
    [quark, anti_quark, gluon],
    [
        VertexExpression(
            FFV,
            Dict(
                color => "color_SU3T(3,2,1)"
            ),
            "-1"
        )
    ],
    ig
)

three_gluon_vertex = Vertex(
    "three_gluon_vertex",
    [gluon, gluon, gluon],
    [
        VertexExpression(
            VVV,
            Dict(
                color => "color_SU3F(1,2,3)"
            ),
            "-1"
        )
    ],
    g
)

four_gluon_vertex = Vertex(
    "four_gluon_vertex",
    [gluon, gluon, gluon, gluon],
    [
        VertexExpression(
            VVVV1,
            Dict(
                color => "color_SU3F(1,2,-1)*color_SU3F(3,4,-1)"
            ),
            "-1"
        ),
        VertexExpression(
            VVVV2,
            Dict(
                color => "color_SU3F(1,3,-1)*color_SU3F(2,4,-1)"
            ),
            "-1"
        ),
        VertexExpression(
            VVVV3,
            Dict(
                color => "color_SU3F(1,4,-1)*color_SU3F(2,3,-1)"
            ),
            "-1"
        )
    ],
    ig_sqr
)

ghost_vertex = Vertex(
    "ghost_vertex",
    [ghost, anti_ghost, gluon],
    [
        VertexExpression(
            GGV,
            Dict(
                color => "color_SU3F(1,3,2)"
            ),
            "-1"
        )
    ],
    g
)

QCDCT_gluon_self_energy = Vertex(
    "QCDCT_gluon_self_energy",
    [gluon, gluon, CT, anti_CT],
    [
        VertexExpression(
            VV,
            Dict(
                color => "color_adjoint_Delta(2,1)"
            ),
            "1"
        )
    ],
    iZ3
)

QCDCT_fermion_self_energy = Vertex(
    "QCDCT_fermion_self_energy",
    [quark, anti_quark, CT, anti_CT],
    [
        VertexExpression(
            FF1,
            Dict(
                color => "color_triplet_Delta(2,1)"
            ),
            "(1+dZ2)"
        ),
        VertexExpression(
            FF2,
            Dict(
                color => "color_triplet_Delta(2,1)"
            ),
            "(1+dZ2)*(1+dZmq)"
        )
    ],
    pure_im
)

QCDCT_ghost_self_energy = Vertex(
    "QCDCT_ghost_self_energy",
    [ghost, anti_ghost, CT, anti_CT],
    [
        VertexExpression(
            GG,
            Dict(
                color => "color_adjoint_Delta(2,1)"
            ),
            "1"
        )
    ],
    iZghost
)

QCDCT_fermion_vertex = CTlize_vertex(fermion_vertex, "QCDCT", CT, ig_Z2_Z3sqrt)
QCDCT_three_gluon_vertex = CTlize_vertex(three_gluon_vertex, "QCDCT", CT, g_Z33over2)
QCDCT_four_gluon_vertex = CTlize_vertex(four_gluon_vertex, "QCDCT", CT, ig_sqr_Z3sqr)
QCDCT_ghost_vertex = CTlize_vertex(ghost_vertex, "QCDCT", CT, g_Zghost_Z3sqrt)

all_vertices = Dict(
    "fermion_vertex" => fermion_vertex,
    "three_gluon_vertex" => three_gluon_vertex,
    "four_gluon_vertex" => four_gluon_vertex,
    "ghost_vertex" => ghost_vertex,

    "QCDCT_gluon_self_energy" => QCDCT_gluon_self_energy,
    "QCDCT_fermion_self_energy" => QCDCT_fermion_self_energy,
    "QCDCT_ghost_self_energy" => QCDCT_ghost_self_energy,

    "QCDCT_fermion_vertex" => QCDCT_fermion_vertex,
    "QCDCT_three_gluon_vertex" => QCDCT_three_gluon_vertex,
    "QCDCT_four_gluon_vertex" => QCDCT_four_gluon_vertex,
    "QCDCT_ghost_vertex" => QCDCT_ghost_vertex
)
