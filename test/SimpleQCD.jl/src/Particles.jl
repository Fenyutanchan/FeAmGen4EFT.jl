# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

quark = Particle(
    "quark",
    2, # u quark
    2,
    [color_triplet],
    mq,
    ZERO,
    Dict("Q" => 2//3) # electric charge
)

anti_quark = anti(quark)

gluon = Particle(
    "gluon",
    21,
    3,
    [color_adjoint],
    ZERO,
    ZERO,
    Dict{String, Real}();
    self_conjugate_flag=true,
    gauge_boson_flag=true
)

ghost = Particle(
    "ghost",
    9999999,
    0, # for Grassmanian scalar
    [color_adjoint],
    ZERO,
    ZERO,
    Dict("ghost" => 1);
    external_flag=false
)
anti_ghost = anti(ghost)

CT = Particle(
    "CT",
    9999998,
    0, # for Grassmanian scalar
    Representation[],
    ZERO,
    ZERO,
    Dict{String, Real}("QCDCT" => 1);
    auxiliary_flag=true,
    external_flag=false
)
anti_CT = anti(CT)

all_particles = Dict(
    "quark" => quark,
    "anti_quark" => anti_quark,
    "gluon" => gluon,
    "ghost" => ghost,
    "anti_ghost" => anti_ghost,
    "CT" => CT,
    "anti_CT" => anti_CT
)
