# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

color_triplet = Representation("color_triplet", color, 3, 3; has_anti=true)
color_adjoint = Representation("color_adjoint", color, 8, 8; has_anti=false)

all_representations = Dict(
    "color_triplet" => color_triplet,
    "color_adjoint" => color_adjoint
)
