# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

color_triplet_Delta = GroupFunction(
    "color_triplet_Delta",
    color,
    [color_triplet, color_triplet]
)

color_adjoint_Delta = GroupFunction(
    "color_adjoint_Delta",
    color,
    [color_adjoint, color_adjoint]
)

color_SU3F = GroupFunction(
    "color_SU3F",
    color,
    [color_adjoint, color_adjoint, color_adjoint]
)

color_SU3T = GroupFunction(
    "color_SU3T",
    color,
    [color_adjoint, color_triplet, color_triplet]
)

all_group_functions = Dict(
    "color_triplet_Delta" => color_triplet_Delta,
    "color_adjoint_Delta" => color_adjoint_Delta,
    "color_SU3F" => color_SU3F,
    "color_SU3T" => color_SU3T
)
