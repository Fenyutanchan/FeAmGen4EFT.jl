# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

FFV = LorentzStructure(
    "FFV",
    [2, 2, 3],
    "Gamma(3,2,1)"
)

VVV = LorentzStructure(
    "VVV",
    [3, 3, 3],
    "Metric(1,2)*(P(1,3)-P(2,3))+Metric(2,3)*(P(2,1)-P(3,1))+Metric(3,1)*(P(3,2)-P(1,2))"
)

VVVV1 = LorentzStructure(
    "VVV1",
    [3, 3, 3, 3],
    "Metric(1,3)*Metric(2,4)-Metric(1,4)*Metric(2,3)"
)

VVVV2 = LorentzStructure(
    "VVV2",
    [3, 3, 3, 3],
    "Metric(1,2)*Metric(3,4)-Metric(1,4)*Metric(2,3)"
)

VVVV3 = LorentzStructure(
    "VVV3",
    [3, 3, 3, 3],
    "Metric(1,2)*Metric(3,4)-Metric(1,3)*Metric(2,4)"
)

GGV = LorentzStructure(
    "GGV",
    [1, 1, 3],
    "P(1,3)"
)

VV = LorentzStructure(
    "VV",
    [3, 3, -1, -1],
    "-Metric(1,2)*P(1,-1)*P(1,-1)+P(1,1)*P(1,2)"
)

FF1 = LorentzStructure(
    "FF1",
    [2, 2, -1, -1],
    "Gamma(-1,2,1)*P(1,-1)"
)

FF2 = LorentzStructure(
    "FF2",
    [2, 2, -1, -1],
    "Identity(2,1)"
)

GG = LorentzStructure(
    "GG",
    [1, 1, -1, -1],
    "P(1,-1)*P(1,-1)"
)
