# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

ZERO = RealInternalParameter("ZERO", 0)

gS = ExternalParameter("gS", "gS")
mq = ExternalParameter("mq", "mq")

# dZ1 = ExternalParameter("dZ1", "dZ1")
dZ2 = ExternalParameter("dZ2", "dZ2")
dZ3 = ExternalParameter("dZ3", "dZ3")
dZghost = ExternalParameter("dZghost", "dZghost")
dZmq = ExternalParameter("dZmq", "dZmq")

all_parameters = Dict(
    "ZERO" => ZERO,
    "gS" => gS,
    "mq" => mq,

    # "dZ1" => dZ1,
    "dZ2" => dZ2,
    "dZ3" => dZ3,
    "dZghost" => dZghost,
    "dZmq" => dZmq
)
