# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

const get_color_form_scripts_dir() = joinpath(
    (dirname ∘ dirname ∘ pathof)(@__MODULE__),
    "ext", "ColorFormScripts"
)

color = Group("color",
    joinpath.(get_color_form_scripts_dir(), [
            "init_definitions.frm",
            "contract_color_indices.frm"
        ]
    )
)

all_groups = Dict(
    "color" => color
)
