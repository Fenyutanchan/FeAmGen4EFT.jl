<!--
 Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>

 This software is released under the MIT License.
 https://opensource.org/licenses/MIT
-->

# `SimpleQCD.jl`

We present this example model for quantum chromodynamics (QCD), where counter-terms are also defined.

We follow the Collins’ *Foundations of perturbative QCD*, where the renormalized QCD Lagrangian reads
$$
\begin{aligned}
	\mathcal{L} & = Z_2 \bar{\psi} (\mathrm{i} \partial\!\!\!/ - m_0) \psi - Z_2 Z_3^{1/2} g_0 \bar{\psi} t^a A\!\!\!/^a \psi \\
	& \quad\quad -\frac{Z_3}{4} (\partial_\mu A^a_\nu - \partial_\nu A^a_\mu)^2 + \frac{Z_3^{1/2} g_0}{2} f^{abc} (\partial_\mu A^a_\nu - \partial_\nu A^a_\mu) A^b_\mu A^c_\nu \\
	& \quad\quad -\frac{Z_3^2 g_0^2}{4} (f^{abc} A^b_\mu A^c_\nu)^2 - \frac{Z_3}{2 \xi_0} (\partial \cdot A^a)^2 \\
	& \quad\quad + \tilde{Z} \partial_\mu \bar{\eta}^a \partial^\mu \eta^a + \tilde{Z} Z_3^{1/2} g_0 f^{abc} \eta^a A^b_\mu \partial^\mu \bar{\eta}^c.
\end{aligned}
$$
