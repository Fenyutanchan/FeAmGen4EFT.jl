<!--
 Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>

 This software is released under the MIT License.
 https://opensource.org/licenses/MIT
-->

# Introduction

Here we present two `Form` scripts for manipulating the color algebra, i.e, the Lie algebra $\mathfrak{su}(3)$ of the Lie group $\mathrm{SU}(3)_\mathrm{color}$. We introduce the following conventions and relations, which are consistent with the Collins’ *Foundations of perturbative QCD*. The Lie algebra $\mathfrak{su}(3)$ satisfied the Lie bracket as
$$
\left[ t^a, t^b \right] = \mathrm{i} f^{abc} t^c,
$$
where $f^{abc}$ is the structure constant. Notice that we apply Einstein sum notation here, and we don’t make a distinction between the covariant and contravariant indices because of Euclidean space.

In the linear representation $R$ of $\mathfrak{su}(3)$ has the same Lie bracket construct, which reads
$$
\left[ t_R^a, t_R^b \right] = \mathrm{i} f^{abc} t_R^c,
$$
where $t_R^a$ is the $D(R)$ dimensional squared matrix with $D(R)$ denoting the dimension of the representation $R$.

The Casimir invariants $T(R)$ and $C(R)$ (sometimes we use $T_R$ and $C_R$ for simplicity) of representation $R$ are defined by
$$
\operatorname{tr}[t_R^a t_R^b] \equiv T(R) \delta^{ab}, \quad t_R^a t_R^a \equiv C(R) \cdot \mathbf{1},
$$
where $\mathbf{1}$ is $D(R) \times D(R)$ identity matrix. They are related by
$$
T(R) D(A) \equiv C(R) D(R),
$$
where $A$ means the adjoint representation, which given by
$$
(t_A^a)^{bc} \equiv -\mathrm{i} f^{abc}.
$$
Warning: Peskin’s QFT textbook adopted the convention of $(t_A^a)^{bc} = \mathrm{i} f^{abc}$.

In these scripts, the fundamental representation $F$ is important, whose dimension is $D(F) = N$ (sometimes we use $N$ instead of $F$ for clarity). We have the following Casimir constants:
$$
\begin{aligned}
	T(F) & = \frac{1}{2}, \quad \text{(this is a convention.)} \\
	C(F) & = \frac{N^2 - 1}{2 N}, \\
	T(A) & = C(A) = N.
\end{aligned}
$$
Notice that $C_A \equiv N$, so we have
$$
C_A \equiv N, \quad C_F \equiv \frac{C_A^2 - 1}{2 C_A}.
$$
Other useful formulae are given as
$$
\begin{aligned}
	t_R^a t_R^b t_R^a & \equiv \left[ C_R - \frac{1}{2} C_A \right] t_R^b, \\
	f^{acd} f^{bcd} & \equiv C_A \delta^{ab}, \\
	f^{abc} t_R^b t_R^c & \equiv \frac{\mathrm{i}}{2} C_A t_R^a, \\
	(t_F^a)_{ij} (t_F^a)_{k\ell} & \equiv T_F \left( \delta_{i\ell} \delta_{kj} - \frac{1}{C_A} \delta_{ij} \delta_{k\ell} \right).
\end{aligned}
$$
Followings are the details that we apply these relations in the `Form` scrips.

# Main Procedure `ContractGroupIndices`

In this procedure, we do the following manipulations for contracting all $\mathfrak{su}(3)$ algebra indices, i.e., the fundamental representation indices and the adjoint representation indices. The workflow of this procedure given as:

1. The code reads
   ```FORM
   repeat;
       #call ContractColorAdjointDeltaSU3F();
       #call ContractColorTripletDeltaSU3T();
       id colorSU3F(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices, colorAdjInd3?coloradjointIndices) *
           colorSU3T(colorAdjind2?coloradjointIndices, colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
           colorSU3T(colorAdjind3?coloradjointIndices, colorTriInd2?colortripletIndices, colorTriInd3?colortripletIndices) =
           (im * CA / 2) * colorSU3T(colorAdjind1, colorTriInd1, colorTriInd3);
   endrepeat;
   .sort
   ```

   where the procedures [`ContractColorAdjointDeltaSU3F`](#ContractColorAdjointDeltaSU3F) and [`ContractColorTripletDeltaSU3T`](#ContractColorTripletDeltaSU3T) are applied, and the relation of
   $$
   f^{abc} t_F^b t_F^c \equiv \frac{\mathrm{i}}{2} C_A t_F^a
   $$
   also be applied. Here, we can minimize the number of $f^{abc}$‘s as much as possible.

2. Rewrite the $f^{abc}$ as (see [`RewriteColorSU3F`](#RewriteColorSU3F))
   $$
   f^{abc} = -2 \mathrm{i} \left( \operatorname{tr}[ t_F^a t_F^b t_F^c ] - \operatorname{tr}[ t_F^b t_F^a t_F^c ] \right);
   $$

3. [`ApplySUNFundamentalRelation`](#ApplySUNFundamentalRelation1) will apply the relation of
   $$
   (t_F^a)_{ij} (t_F^a)_{k\ell} \equiv T_F \left( \delta_{i\ell} \delta_{kj} - \frac{1}{C_A} \delta_{ij} \delta_{k\ell} \right);
   $$
   This is a powerful relation for simplification, and we will call it again later.

4. Construct the product chain or trace of (representation-$F$) generators (see [`ConstructColorSU3TChainAndTrace`](#ConstructColorSU3TChainAndTrace));

5. Do [`ApplySUNFundamentalRelation`](#ApplySUNFundamentalRelation1) again;

6. `im` is the imaginary unit, so `id im^2 = -1;` finally.

# <div id="ApplySUNFundamentalRelation"> Procedure `ApplySUNFundamentalRelation`</div>

The code reads

```FORM
#procedure ApplySUNFundamentalRelation()
    repeat;
        id colorSU3T(?vars1, colorAdjInd1?coloradjointIndices, ?vars2, colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
            colorSU3T(?vars3, colorAdjInd1?coloradjointIndices, ?vars4, colorTriInd3?colortripletIndices, colorTriInd4?colortripletIndices) =
            (1/2) * (
                colorSU3T(?vars1, ?vars4, colorTriInd1, colorTriInd4) *
                colorSU3T(?vars3, ?vars2, colorTriInd3, colorTriInd2) -
                (1/CA) * colorSU3T(?vars1, ?vars2, colorTriInd1, colorTriInd2) *
                colorSU3T(?vars3, ?vars4, colorTriInd3, colorTriInd4)
            );
        id colorSU3T(?vars1, colorAdjInd1?coloradjointIndices, ?vars2, colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
            colorSU3Trace(?vars3, colorAdjInd1?coloradjointIndices, ?vars4) =
            (1/2) * (
                colorSU3T(?vars1, ?vars4, ?vars3, ?vars2, colorTriInd1, colorTriInd2) -
                (1/CA) * colorSU3T(?vars1, ?vars2, colorTriInd1, colorTriInd2) *
                colorSU3Trace(?vars3, ?vars4)
            );
        id colorSU3Trace(?vars1, colorAdjInd1?coloradjointIndices, ?vars2) *
            colorSU3Trace(?vars3, colorAdjInd1?coloradjointIndices, ?vars4) =
            (1/2) * (
                colorSU3Trace(?vars1, ?vars4, ?vars3, ?vars2) -
                (1/CA) * colorSU3Trace(?vars1, ?vars2) *
                colorSU3Trace(?vars3, ?vars4)
            );

        id colorSU3T(colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) =
            colortripletDelta(colorTriInd1, colorTriInd2);

        id colorSU3Trace(colorAdjInd1?coloradjointIndices) = 0;
        id colorSU3Trace() = CA;

        #call ContractColorAdjointDeltaSU3T();
        #call ContractColorTripletDeltaSU3T();
    endrepeat;
    .sort
#endprocedure
```

where we apply the powerful relation of
$$
(t_F^a)_{ij} (t_F^a)_{k\ell} \equiv T_F \left( \delta_{i\ell} \delta_{kj} - \frac{1}{C_A} \delta_{ij} \delta_{k\ell} \right)
$$
for simplification. In details, there are three types shown as follows:
$$
\begin{aligned}
	\left( t_F^{(\cdots)_1 a (\cdots)_2} \right)_{i j} \left( t_F^{(\cdots)_3 a (\cdots)_4} \right)_{k \ell} & = T_F \left[ \left( t_F^{(\cdots)_1 (\cdots)_4} \right)_{i \ell} \left( t_F^{(\cdots)_3 (\cdots)_2} \right)_{k j} - \frac{1}{C_A} \left( t_F^{(\cdots)_1 (\cdots)_2} \right)_{i j} \left( t_F^{(\cdots)_3 (\cdots)_4} \right)_{k \ell} \right]; \\
	\left( t_F^{(\cdots)_1 a (\cdots)_2} \right)_{i j} \operatorname{tr} t_F^{(\cdots)_3 a (\cdots)_4} & = T_F \left[ \operatorname{tr} t_F^{(\cdots)_1 (\cdots)_4 (\cdots)_3 (\cdots)_2} - \frac{1}{C_A} \left( t_F^{(\cdots)_1 (\cdots)_2} \right)_{i j} \operatorname{tr} t_F^{(\cdots)_3 (\cdots)_4} \right]; \\
	\operatorname{tr} t_F^{(\cdots)_1 a (\cdots)_2} \operatorname{tr} t_F^{(\cdots)_3 a (\cdots)_4} & = T_F \left[ \operatorname{tr} t_F^{(\cdots)_1 (\cdots)_4 (\cdots)_3 (\cdots)_2} - \frac{1}{C_A} \operatorname{tr} t_F^{(\cdots)_1 (\cdots)_2} \operatorname{tr} t_F^{(\cdots)_3 (\cdots)_4} \right].
\end{aligned}
$$
Notice that
$$
\operatorname{tr} t_F^a \equiv 0, \quad\text{and}\quad \operatorname{tr} \left[ I_{D(F) \times D(F)} \right] = D(F) = N = C_A.
$$
The procedures [`ContractColorAdjointDeltaSU3T`](#ContractColorAdjointDeltaSU3T) and [`ContractColorTripletDeltaSU3T`](#ContractColorTripletDeltaSU3T) are also required.

# <div id="ConstructColorSU3TChainAndTrace">Procedure `ConstructColorSU3TChainAndTrace`</div>

Here we contract the generators of fundamental representation iteratively as
$$
\left( t_F^a \right)_{ij} \left( t_F^b \right)_{jk} = \left( t_F^{ab} \right)_{ik}, \quad\text{and}\quad \left( t_F^{a \cdots b} \right)_{ij} \left( t_F^c \right)_{jk} = \left( t_F^{a \cdots b c} \right)_{ik}
$$
which is shown as `colorSU3TChain(a, …, b, c, i, k)` finally.

The trace is constructed as
$$
\left( t_F^{a \cdots b} \right)_{ii} = \operatorname{tr} t_F^{a \cdots b},
$$
which is shown as `colorSU3Trace(a, …, b)` finally.

The corresponding code reads
```FORM
#procedure ConstructColorSU3TChainAndTrace()
    repeat;
        id colorSU3T(?vars1, colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
            colorSU3T(?vars2, colorTriInd2?colortripletIndices, colorTriInd3?colortripletIndices) =
            colorSU3T(?vars1, ?vars2, colorTriInd1, colorTriInd3);
        #call ContractColorAdjointDeltaSU3T();
        #call ContractColorTripletDeltaSU3T();
        id colorSU3T(?vars, colorTriInd1?colortripletIndices, colorTriInd1?colortripletIndices) = colorSU3Trace(?vars);

        #call ContractColorAdjointDeltaSU3Trace();
    endrepeat;
    .sort
#endprocedure
```

The related procedures are [`ContractColorAdjointDeltaSU3T`](#ContractColorAdjointDeltaSU3T), [`ContractColorTripletDeltaSU3T`](#ContractColorTripletDeltaSU3T) and [`ContractColorAdjointDeltaSU3Trace`](#ContractColorAdjointDeltaSU3Trace).

# <div id="ContractColorAdjointDeltaBasic">Procedure `ContractColorAdjointDeltaBasic`</div>

Please read source code directly.

# <div id="ContractColorAdjointDeltaSU3F">Procedure `ContractColorAdjointDeltaSU3F`</div>

Please read source code directly.

# <div id="ContractColorAdjointDeltaSU3T">Procedure `ContractColorAdjointDeltaSU3T`</div>

Please read source code directly.

# <div id="ContractColorAdjointDeltaSU3Trace">Procedure `ContractColorAdjointDeltaSU3Trace`</div>

Please read source code directly.

# <div id="ContractColorTripletDeltaBasic">Procedure `ContractColorTripletDeltaBasic`</div>

Please read source code directly.

# <div id="ContractColorTripletDeltaSU3T">Procedure `ContractColorTripletDeltaSU3T`</div>

Please read source code directly.

# <div id="RewriteColorSU3F">Procedure `RewriteColorSU3F`</div>

Notice that
$$
\begin{aligned}
	\operatorname{tr} \left[ \left[ t_R^a, t_R^b \right], t_R^c \right] & = \operatorname{tr} \left[ \mathrm{i} f^{abd} t_R^d t_R^c \right] \\
	& = \mathrm{i} f^{abd} \operatorname{tr} \left[ t_R^d t_R^c \right] \\
	& = \mathrm{i} T_R f^{abd} \delta^{dc} \\
	& = \mathrm{i} T_R f^{abc}.
\end{aligned}
$$
Therefore,
$$
f^{abc} = \frac{-\mathrm{i}}{T_R} \operatorname{tr} \left[ \left[ t_R^a, t_R^b \right], t_R^c \right].
$$
Especially for $R = F$, we have $T_F = 1/2$ as convention, then
$$
f^{abc} = -2 \mathrm{i} \left( \operatorname{tr}[ t_R^a t_R^b t_R^c] - \operatorname{tr}[ t_R^b t_R^a t_R^c] \right).
$$
That is what the code reads

```FORM
#procedure RewriteColorSU3F()
    id colorSU3F(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices, colorAdjInd3?coloradjointIndices) =
        -2 * im * (
            colorSU3Trace(colorAdjInd1, colorAdjInd2, colorAdjInd3) -
            colorSU3Trace(colorAdjInd2, colorAdjInd1, colorAdjInd3)
        );
#endprocedure
```

