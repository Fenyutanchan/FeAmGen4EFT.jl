* Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
* 
* This software is released under the MIT License.
* https://opensource.org/licenses/MIT

#procedure ContractGroupIndices()
    repeat;
        #call ContractColorAdjointDeltaSU3F();
        #call ContractColorTripletDeltaSU3T();
        id colorSU3F(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices, colorAdjInd3?coloradjointIndices) *
            colorSU3T(colorAdjInd2?coloradjointIndices, colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
            colorSU3T(colorAdjInd3?coloradjointIndices, colorTriInd2?colortripletIndices, colorTriInd3?colortripletIndices) =
            (im * CA / 2) * colorSU3T(colorAdjInd1, colorTriInd1, colorTriInd3);
    endrepeat;
    .sort
    #call RewriteColorSU3F();
    .sort

    #call ApplySUNFundamentalRelation();

    #call ConstructColorSU3TChainAndTrace();

    #call ApplySUNFundamentalRelation();

    id im^2 = -1;
    .sort
#endprocedure

#procedure ApplySUNFundamentalRelation()
    repeat;
        id colorSU3T(?vars1, colorAdjInd1?coloradjointIndices, ?vars2, colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
            colorSU3T(?vars3, colorAdjInd1?coloradjointIndices, ?vars4, colorTriInd3?colortripletIndices, colorTriInd4?colortripletIndices) =
            (1/2) * (
                colorSU3T(?vars1, ?vars4, colorTriInd1, colorTriInd4) *
                colorSU3T(?vars3, ?vars2, colorTriInd3, colorTriInd2) -
                (1/CA) * colorSU3T(?vars1, ?vars2, colorTriInd1, colorTriInd2) *
                colorSU3T(?vars3, ?vars4, colorTriInd3, colorTriInd4)
            );
        id colorSU3T(?vars1, colorAdjInd1?coloradjointIndices, ?vars2, colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
            colorSU3Trace(?vars3, colorAdjInd1?coloradjointIndices, ?vars4) =
            (1/2) * (
                colorSU3T(?vars1, ?vars4, ?vars3, ?vars2, colorTriInd1, colorTriInd2) -
                (1/CA) * colorSU3T(?vars1, ?vars2, colorTriInd1, colorTriInd2) *
                colorSU3Trace(?vars3, ?vars4)
            );
        id colorSU3Trace(?vars1, colorAdjInd1?coloradjointIndices, ?vars2) *
            colorSU3Trace(?vars3, colorAdjInd1?coloradjointIndices, ?vars4) =
            (1/2) * (
                colorSU3Trace(?vars1, ?vars4, ?vars3, ?vars2) -
                (1/CA) * colorSU3Trace(?vars1, ?vars2) *
                colorSU3Trace(?vars3, ?vars4)
            );

        id colorSU3T(colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) =
            colortripletDelta(colorTriInd1, colorTriInd2);

        id colorSU3Trace(colorAdjInd1?coloradjointIndices) = 0;
        id colorSU3Trace() = CA;

        #call ContractColorAdjointDeltaSU3T();
        #call ContractColorTripletDeltaSU3T();
    endrepeat;
    .sort
#endprocedure

#procedure ConstructColorSU3TChainAndTrace()
    repeat;
        id colorSU3T(?vars1, colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
            colorSU3T(?vars2, colorTriInd2?colortripletIndices, colorTriInd3?colortripletIndices) =
            colorSU3T(?vars1, ?vars2, colorTriInd1, colorTriInd3);
        #call ContractColorAdjointDeltaSU3T();
        #call ContractColorTripletDeltaSU3T();
        id colorSU3T(?vars, colorTriInd1?colortripletIndices, colorTriInd1?colortripletIndices) = colorSU3Trace(?vars);

        #call ContractColorAdjointDeltaSU3Trace();
    endrepeat;
    .sort
#endprocedure

#procedure ContractColorAdjointDeltaBasic()
    id coloradjointDelta(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices) *
        coloradjointDelta(colorAdjInd2?coloradjointIndices, colorAdjInd3?coloradjointIndices) =
        coloradjointDelta(colorAdjInd1, colorAdjInd3);
    id coloradjointDelta(colorAdjInd1?coloradjointIndices, colorAdjInd1?coloradjointIndices) = 2 * CA * CF;
#endprocedure

#procedure ContractColorAdjointDeltaSU3F()
    #call ContractColorAdjointDeltaBasic();

    id colorSU3F(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices, colorAdjInd3?coloradjointIndices) *
        colorSU3F(colorAdjInd4?coloradjointIndices, colorAdjInd2?coloradjointIndices, colorAdjInd3?coloradjointIndices) =
        CA * coloradjointDelta(colorAdjInd1, colorAdjInd4);
    id colorSU3F(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices, colorAdjInd3?coloradjointIndices) *
        coloradjointDelta(colorAdjInd3?coloradjointIndices, colorAdjInd4?coloradjointIndices) =
        colorSU3F(colorAdjInd1, colorAdjInd2, colorAdjInd4);
#endprocedure

#procedure ContractColorAdjointDeltaSU3T()
    #call ContractColorAdjointDeltaBasic();

    id colorSU3T(?vars1, colorAdjInd1?coloradjointIndices, ?vars2) *
        coloradjointDelta(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices) =
        colorSU3T(?vars1, colorAdjInd2, ?vars2);
#endprocedure

#procedure ContractColorAdjointDeltaSU3Trace()
    #call ContractColorAdjointDeltaBasic();

    id colorSU3Trace(?vars1, colorAdjInd1?coloradjointIndices, ?vars2) *
        coloradjointDelta(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices) =
        colorSU3Trace(?vars1, colorAdjInd2, ?vars2);
#endprocedure

#procedure ContractColorTripletDeltaBasic()
    id colortripletDelta(colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) *
        colortripletDelta(colorTriInd2?colortripletIndices, colorTriInd3?colortripletIndices) =
        colortripletDelta(colorTriInd1, colorTriInd3);
    id colortripletDelta(colorTriInd1?colortripletIndices, colorTriInd1?colortripletIndices) = CA;
#endprocedure

#procedure ContractColorTripletDeltaSU3T()
    #call ContractColorTripletDeltaBasic();

    id colorSU3T(?vars1, colorTriInd1?colortripletIndices, ?vars2) *
        colortripletDelta(colorTriInd1?colortripletIndices, colorTriInd2?colortripletIndices) =
        colorSU3T(?vars1, colorTriInd2, ?vars2);
#endprocedure

#procedure RewriteColorSU3F()
    id colorSU3F(colorAdjInd1?coloradjointIndices, colorAdjInd2?coloradjointIndices, colorAdjInd3?coloradjointIndices) =
        -2 * im * (
            colorSU3Trace(colorAdjInd1, colorAdjInd2, colorAdjInd3) -
            colorSU3Trace(colorAdjInd2, colorAdjInd1, colorAdjInd3)
        );
#endprocedure
