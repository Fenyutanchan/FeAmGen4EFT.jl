* Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
* 
* This software is released under the MIT License.
* https://opensource.org/licenses/MIT

CFunction colortripletDelta(symmetric);
CFunction coloradjointDelta(symmetric);
CFunction colorSU3F(antisymmetric), colorSU3T;

* internal color indices
Auto Symbol colorTriInd, colorAdjInd;

CFunction colorSU3Trace(cyclesymmetric);

* results
Symbol CA, CF, im;
