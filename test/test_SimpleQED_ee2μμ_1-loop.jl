# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

import SimpleQED

using FeAmGen4EFT

FeAmGen4EFT.run_Qgraf(
    SimpleQED,
    1,
    ["electron", "anti_electron"],
    ["muon", "anti_muon"],
    ["notadpole", "onshell"];
    order_list=Dict("QED" => 4)
)

all_Feynman_diagrams = FeAmGen4EFT.analyze_Qgraf_output(
    SimpleQED,
    joinpath(@__DIR__, "qgraf_output.out")
)

one_Feynman_diagram = first(all_Feynman_diagrams)
one_vertex = first(one_Feynman_diagram.internal_vertex_list)
@show FeAmGen4EFT.analyze_vertex_structure(SimpleQED, one_Feynman_diagram, one_vertex)

one_propagator = first(one_Feynman_diagram.propagator_list)
one_external_leg = first(one_Feynman_diagram.external_leg_list)
@show FeAmGen4EFT.analyze_propagator_structure(one_Feynman_diagram, one_propagator)
@show FeAmGen4EFT.analyze_external_leg_structure(one_Feynman_diagram, one_external_leg)

@show FeAmGen4EFT.generate_amplitude(SimpleQED, one_Feynman_diagram)

rm("Feynman_diagrams.log"; force=true, recursive=true)
open("Feynman_diagrams.log", "w+") do io
    for Feynman_diagram ∈ all_Feynman_diagrams
        write(io, "$Feynman_diagram\n")
    end
end
