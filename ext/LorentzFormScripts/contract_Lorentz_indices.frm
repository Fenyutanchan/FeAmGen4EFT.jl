* Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
* 
* This software is released under the MIT License.
* https://opensource.org/licenses/MIT

#procedure ContractLorentzIndices()
    #call ContractMT();
    #call ContractMomenta();
    #call SimplifyTransversePolarizationVector();
#endprocedure

#procedure ContractMT()
    repeat;
        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            MT(LorentzInd2?LorentzIndices, LorentzInd3?LorentzIndices) =
            MT(LorentzInd1, LorentzInd3);
        id MT(LorentzInd1?LorentzIndices, LorentzInd1?LorentzIndices) = 4;

        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            FV(MomentumP?, LorentzInd2?LorentzIndices) =
            FV(MomentumP, LorentzInd1);

        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            VecEps(MomentumP?, MassM?Mass, LorentzInd2?LorentzIndices) =
            VecEps(MomentumP, MassM, LorentzInd1);
        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            VecEpsConj(MomentumP?, MassM?Mass, LorentzInd2?LorentzIndices) =
            VecEpsConj(MomentumP, MassM, LorentzInd1);

        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            VecEps(MomentumP?, 0, LorentzInd2?LorentzIndices) =
            VecEps(MomentumP, LorentzInd1);
        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            VecEpsConj(MomentumP?, 0, LorentzInd2?LorentzIndices) =
            VecEpsConj(MomentumP, LorentzInd1);
        
        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            FermionChain(?vars1, GA(LorentzInd2?LorentzIndices), ?vars2) =
            FermionChain(?vars1, GA(LorentzInd1), ?vars2);
        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            FermionLoopTrace(?vars1, GA(LorentzInd2?LorentzIndices), ?vars2) =
            FermionLoopTrace(?vars1, GA(LorentzInd1), ?vars2);
    endrepeat;
    .sort
#endprocedure

#procedure ContractMomenta()
    repeat;
        id FV(MomentumP1?, LorentzInd1?LorentzIndices) *
            FV(MomentumP2?, LorentzInd1?LorentzIndices) =
            SP(MomentumP1, MomentumP2);
        id SP(MomentumP?, MomentumP?) = SP(MomentumP);
        id SP(MomentumP?MasslessNonLoopMomenta) = 0;
    endrepeat;
    .sort
#endprocedure

#procedure SimplifyTransversePolarizationVector()
    repeat;
        id VecEps(MomentumP?, 0, LorentzInd1?LorentzIndices) =
            VecEps(MomentumP, LorentzInd1);
        id VecEpsConj(MomentumP?, 0, LorentzInd1?LorentzIndices) =
            VecEpsConj(MomentumP, LorentzInd1);

        id VecEps(MomentumP?, LorentzInd1?LorentzIndices) *
            FV(MomentumP?, LorentzInd1?LorentzIndices) = 0;
        id VecEpsConj(MomentumP?, LorentzInd1?LorentzIndices) *
            FV(MomentumP?, LorentzInd1?LorentzIndices) = 0;
    endrepeat;
    .sort
#endprocedure
