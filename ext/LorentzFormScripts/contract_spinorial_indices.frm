* Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
* 
* This software is released under the MIT License.
* https://opensource.org/licenses/MIT

#procedure ContractSpinorialIndices()
    repeat;
        #call SimpleContractGA5ij();
        id ChargeConjC(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            -SpinDelta(spinorialInd1, spinorialInd3);
        #call ContractSpinDelta();
    endrepeat;
    .sort

    #call ConstructFermionChain();
    #call ConstructFermionLoopTrace();
#endprocedure

#procedure SimpleContractGA5ij()
    id GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        GA5ij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        SpinDelta(spinorialInd1, spinorialInd3);
    id GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        ProjMij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        -ProjMij(spinorialInd1, spinorialInd3);
    id GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        ProjPij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        ProjPij(spinorialInd1, spinorialInd3);
    id ProjMij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        ProjMij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        ProjMij(spinorialInd1, spinorialInd3);
    id ProjPij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        ProjPij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        ProjPij(spinorialInd1, spinorialInd3);
    id ProjMij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        ProjPij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) = 0;
#endprocedure

#procedure ContractSpinDelta()
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd1?SpinorialIndices) = 4;
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinDelta(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        SpinDelta(spinorialInd1, spinorialInd3);

    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        GAij(LorentzInd1?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        GAij(LorentzInd1, spinorialInd1, spinorialInd3);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        GAij(LorentzInd1?LorentzIndices, spinorialInd3?SpinorialIndices, spinorialInd2?SpinorialIndices) =
        GAij(LorentzInd1, spinorialInd3, spinorialInd1);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        GA5ij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        GA5ij(spinorialInd1, spinorialInd3);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        ProjMij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        ProjMij(spinorialInd1, spinorialInd3);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        ProjPij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        ProjPij(spinorialInd1, spinorialInd3);

    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinorUBar(spinorialInd2?SpinorialIndices, MomentumP?, MassM?Mass) =
        SpinorUBar(spinorialInd1, MomentumP, MassM);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinorU(spinorialInd2?SpinorialIndices, MomentumP?, MassM?Mass) =
        SpinorU(spinorialInd1, MomentumP, MassM);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinorVBar(spinorialInd2?SpinorialIndices, MomentumP?, MassM?Mass) =
        SpinorVBar(spinorialInd1, MomentumP, MassM);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinorV(spinorialInd2?SpinorialIndices, MomentumP?, MassM?Mass) =
        SpinorV(spinorialInd1, MomentumP, MassM);

    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinorUBar(spinorialInd2?SpinorialIndices, MomentumP?, 0) =
        SpinorUBar(spinorialInd1, MomentumP, 0);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinorU(spinorialInd2?SpinorialIndices, MomentumP?, 0) =
        SpinorU(spinorialInd1, MomentumP, 0);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinorVBar(spinorialInd2?SpinorialIndices, MomentumP?, 0) =
        SpinorVBar(spinorialInd1, MomentumP, 0);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        SpinorV(spinorialInd2?SpinorialIndices, MomentumP?, 0) =
        SpinorV(spinorialInd1, MomentumP, 0);

    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        ChargeConjC(spinorialInd1, spinorialInd3);

    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
        GSij(MomentumP, MassM, spinorialInd1, spinorialInd3);
    id SpinDelta(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
        GSij(MomentumP?, MassM?Mass, spinorialInd3?SpinorialIndices, spinorialInd2?SpinorialIndices) =
        GSij(MomentumP, MassM, spinorialInd3, spinorialInd1);
#endprocedure

#procedure ConstructFermionChain()
    repeat;
        id SpinorUBar(spinorialInd1?SpinorialIndices, MomentumP?, MassM?Mass) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(SpinorUBar(MomentumP, MassM), GA(LorentzInd1), spinorialInd2);
        id SpinorVBar(spinorialInd1?SpinorialIndices, MomentumP?, MassM?Mass) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(SpinorVBar(MomentumP, MassM), GA(LorentzInd1), spinorialInd2);

        id SpinorUBar(spinorialInd1?SpinorialIndices, MomentumP?, 0) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(SpinorU(MomentumP), GA(LorentzInd1), spinorialInd2);
        id SpinorVBar(spinorialInd1?SpinorialIndices, MomentumP?, 0) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(SpinorV(MomentumP), GA(LorentzInd1), spinorialInd2);

        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(?vars, GA(LorentzInd1), spinorialInd2);
        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(?vars, GA5, spinorialInd2);
        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            ProjMij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(?vars, PL, spinorialInd2);
        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            ProjPij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(?vars, PR, spinorialInd2);
        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            ChargeConjC(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(?vars, CC, spinorialInd2);

        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(?vars, GSAddMass(GS(MomentumP), MassM), spinorialInd2);
        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) =
            FermionChain(?vars, GS(MomentumP), spinorialInd2);

        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            SpinorU(spinorialInd1?SpinorialIndices, MomentumP?, MassM?Mass) =
            FermionChain(?vars, SpinorU(MomentumP, MassM));
        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            SpinorV(spinorialInd1?SpinorialIndices, MomentumP?, MassM?Mass) =
            FermionChain(?vars, SpinorV(MomentumP, MassM));

        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            SpinorU(spinorialInd1?SpinorialIndices, MomentumP?, 0) =
            FermionChain(?vars, SpinorU(MomentumP));
        id FermionChain(?vars, spinorialInd1?SpinorialIndices) *
            SpinorV(spinorialInd1?SpinorialIndices, MomentumP?, 0) =
            FermionChain(?vars, SpinorV(MomentumP));

        id MT(LorentzInd1?LorentzIndices, LorentzInd2?LorentzIndices) *
            FermionChain(?vars1, GA(LorentzInd2?LorentzIndices), ?vars2) =
            FermionChain(?vars1, GA(LorentzInd1), ?vars2);
        id FV(MomentumP?, LorentzInd1?LorentzIndices) *
            FermionChain(?vars1, GA(LorentzInd1?LorentzIndices), ?vars2) =
            FermionChain(?vars1, GS(MomentumP), ?vars2);
    endrepeat;
    .sort
#endprocedure

#procedure ConstructFermionLoopTrace()
    repeat;
        id GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GAij(LorentzInd2?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA(LorentzInd1), GA(LorentzInd2), spinorialInd1, spinorialInd3);
        id GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GA5ij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA(LorentzInd1), GA5, spinorialInd1, spinorialInd3);
        id GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ProjMij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA(LorentzInd1), PL, spinorialInd1, spinorialInd3);
        id GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ProjPij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA(LorentzInd1), PR, spinorialInd1, spinorialInd3);
        id GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA(LorentzInd1), CC, spinorialInd1, spinorialInd3);

        id GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA(LorentzInd1), GSAddMass(GS(MomentumP), MassM), spinorialInd1, spinorialInd3);
        id GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA(LorentzInd1), GS(MomentumP), spinorialInd1, spinorialInd3);
    endrepeat;
    .sort

    repeat;
        id GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA5, GA(LorentzInd1), spinorialInd1, spinorialInd3);
        id GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA5, CC, spinorialInd1, spinorialInd3);
        
        id GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA5, GSAddMass(GS(MomentumP), MassM), spinorialInd1, spinorialInd3);
        id GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA5, GS(MomentumP), spinorialInd1, spinorialInd3);
    endrepeat;
    .sort

    repeat;
        id ProjMij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PL, GA(LorentzInd1), spinorialInd1, spinorialInd3);
        id ProjMij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PL, CC, spinorialInd1, spinorialInd3);

        id ProjMij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PL, GSAddMass(GS(MomentumP), MassM), spinorialInd1, spinorialInd3);
        id ProjMij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PL, GS(MomentumP), spinorialInd1, spinorialInd3);

        id ProjPij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PR, GA(LorentzInd1), spinorialInd1, spinorialInd3);
        id ProjPij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PR, CC, spinorialInd1, spinorialInd3);

        id ProjPij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PR, GSAddMass(GS(MomentumP), MassM), spinorialInd1, spinorialInd3);
        id ProjPij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PR, GS(MomentumP), spinorialInd1, spinorialInd3);
    endrepeat;
    .sort

    repeat;
        id ChargeConjC(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(CC, GA(LorentzInd1), spinorialInd1, spinorialInd3);

        id ChargeConjC(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(CC, GSAddMass(GS(MomentumP), MassM), spinorialInd1, spinorialInd3);
        id ChargeConjC(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(CC, GS(MomentumP), spinorialInd1, spinorialInd3);
    endrepeat;
    .sort

    repeat;
        id GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GSAddMass(GS(MomentumP), MassM), GA(LorentzInd1), spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GS(MomentumP), GA(LorentzInd1), spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GA5ij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GSAddMass(GS(MomentumP), MassM), GA5, spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GA5ij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GS(MomentumP), GA5, spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ProjMij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GSAddMass(GS(MomentumP), MassM), PL, spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ProjMij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GS(MomentumP), PL, spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ProjPij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GSAddMass(GS(MomentumP), MassM), PR, spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ProjPij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GS(MomentumP), PR, spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GSAddMass(GS(MomentumP), MassM), CC, spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GS(MomentumP), CC, spinorialInd1, spinorialInd3);

        id GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GSAddMass(GS(MomentumP), MassM), GSAddMass(GS(MomentumP), MassM), spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GSAddMass(GS(MomentumP), MassM), GS(MomentumP), spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GS(MomentumP), GSAddMass(GS(MomentumP), MassM), spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GS(MomentumP), GS(MomentumP), spinorialInd1, spinorialInd3);
    endrepeat;
    .sort

    repeat;
        id GAij(LorentzInd1?LorentzIndices, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            FermionLoopChain(?vars, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA(LorentzInd1), ?vars, spinorialInd1, spinorialInd3);
        id GA5ij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            FermionLoopChain(?vars, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GA5, ?vars, spinorialInd1, spinorialInd3);
        id ProjMij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            FermionLoopChain(?vars, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PL, ?vars, spinorialInd1, spinorialInd3);
        id ProjPij(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            FermionLoopChain(?vars, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(PR, ?vars, spinorialInd1, spinorialInd3);
        id ChargeConjC(spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            FermionLoopChain(?vars, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(CC, ?vars, spinorialInd1, spinorialInd3);

        id GSij(MomentumP?, MassM?Mass, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            FermionLoopChain(?vars, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GSAddMass(GS(MomentumP), MassM), ?vars, spinorialInd1, spinorialInd3);
        id GSij(MomentumP?, 0, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            FermionLoopChain(?vars, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(GS(MomentumP), ?vars, spinorialInd1, spinorialInd3);
    endrepeat;
    .sort

    repeat;
        id FermionLoopChain(?vars1, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GAij(LorentzInd1?LorentzIndices, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(?vars1, GA(LorentzInd1), spinorialInd1, spinorialInd3);
        id FermionLoopChain(?vars, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GA5ij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(?vars, GA5, spinorialInd1, spinorialInd3);
        id FermionLoopChain(?vars, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ProjMij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(?vars, PL, spinorialInd1, spinorialInd3);
        id FermionLoopChain(?vars, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ProjPij(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(?vars, PR, spinorialInd1, spinorialInd3);
        id FermionLoopChain(?vars, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            ChargeConjC(spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(?vars, CC, spinorialInd1, spinorialInd3);

        id FermionLoopChain(?vars, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, MassM?Mass, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(?vars, GSAddMass(GS(MomentumP), MassM), spinorialInd1, spinorialInd3);
        id FermionLoopChain(?vars, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            GSij(MomentumP?, 0, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(?vars, GS(MomentumP), spinorialInd1, spinorialInd3);
    endrepeat;
    .sort

    repeat;
        id FermionLoopChain(?vars1, spinorialInd1?SpinorialIndices, spinorialInd2?SpinorialIndices) *
            FermionLoopChain(?vars2, spinorialInd2?SpinorialIndices, spinorialInd3?SpinorialIndices) =
            FermionLoopChain(?vars1, ?vars2, spinorialInd1, spinorialInd3);
        id FermionLoopChain(?vars, spinorialInd1?SpinorialIndices, spinorialInd1?SpinorialIndices) =
            FermionLoopTrace(?vars);
    endrepeat;
    .sort
#endprocedure
