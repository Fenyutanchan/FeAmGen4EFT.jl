* Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
* 
* This software is released under the MIT License.
* https://opensource.org/licenses/MIT

* The function list from the input expression *********************************
CFunction MT(symmetric), FV;

CFunction SpinDelta(symmetric);
CFunction GAij, GA5ij(symmetric), GSij, ProjMij(symmetric), ProjPij(symmetric);
CFunction ChargeConjC(antisymmetric);

CFunction SpinorU, SpinorUBar, SpinorV, SpinorVBar;
CFunction VecEps, VecEpsConj;
*******************************************************************************

* The function and symbol list for internal use *******************************
CFunction FermionLoopChain;
Symbol MassM;

Auto Symbol LorentzInd;
Auto Symbol spinorialInd;

Auto Vector MomentumP;
*******************************************************************************

* The function and symbol list for the output expression **********************
CFunction FermionChain, FermionLoopTrace(cyclesymmetric);
CFunction GA, GS, GSAddMass;
CFunction SP(symmetric);

Symbol CC, GA5, PR, PL;
*******************************************************************************
