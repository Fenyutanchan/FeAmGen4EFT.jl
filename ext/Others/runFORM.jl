using FORM_jll

for filename ∈ ARGS
    if !isfile(filename)
        @warn "File not found: $filename"
        continue
    end

    run(`$(form()) -q $filename`)
end
