# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

using Inflate, SHA, Tar

for filename ∈ ARGS
    if !isfile(filename)
        @warn "File $filename not found."
        continue
    end

    sha1 = try
        (Tar.tree_hash ∘ IOBuffer ∘ inflate_gzip)(filename)
    catch
        @warn "$filename is not a .tar.gz file."
        "N/A"
    end

    @info """
    Hash for $filename:
        git-tree-sha1: $(sha1)
        sha256: $((bytes2hex ∘ open)(sha256, filename))
    """
end
