<!--
 Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
 
 This software is released under the MIT License.
 https://opensource.org/licenses/MIT
-->

# Introduction
Just a mini derivative version of `FeAmGen` for SMEFT one-loop matching, especially designed for [`ABC4EFT`](https://github.com/XiaoMing-lei/ABC4EFT).

# Features
- [x] Support the newly designed model format written in the Julia programming language, see [`UniversalFeynmanModel.jl`](https://code.ihep.ac.cn/wuquanfeng/UniversalFeynmanModel.jl.git). It is similar to the original Universal FeynRules/Feynman Output (UFO) model format in Python (especially Python2) programming langruage;
- [x] Support the newly designed FeynmanGraph structure, see [`FeynmanGraphs.jl`](https://code.ihep.ac.cn/wuquanfeng/FeynmanGraphs.jl.git);
- [x] Re-implement the conter-term generation which is slightly buggy in the original [`FeAmGen.jl`](https://code.ihep.ac.cn/IHEP-Multiloop/FeAmGen.jl) package. Notice that the original [`FeAmGen.jl`](https://code.ihep.ac.cn/IHEP-Multiloop/FeAmGen.jl) package takes the generation of the QCD counter-terms internally, which is hardly generalizable to other models. I propose to generate the counter-terms respect to the model file in this implementation, see `./test/SimpleQCD.jl` model and the corresponding test files `./test/test_SimpleQCD_*.jl` for more details.

# Contributors
- Zhao Li (李钊) <zhaoli@ihep.ac.cn>: The author of [`FeAmGen.jl`](https://code.ihep.ac.cn/IHEP-Multiloop/FeAmGen.jl).
- **Quan-feng WU (吴泉锋)** <wuquanfeng@ihep.ac.cn>: The maintainer of [`FeAmGen.jl`](https://code.ihep.ac.cn/IHEP-Multiloop/FeAmGen.jl) and the author of this package.

# Acknowledgements
I would like to thank Chengjie Yang (杨成杰) <yangchengjie@ihep.ac.cn> and Jiang-Hao Yu (于江浩) <jhyu@itp.ac.cn>, who suggest the development of this package.
They also help me to test this package and provide some useful suggestions.
Chengjie Yang, for especially, provided one test model file for early stage development and tested this package with various models, where he found some fatal bugs.
I am very grateful to him for his help and support.
I would also like to thank Long-Bin Chen (陈龙斌) @ Guangzhou University, Hai-Tao Li (李海涛) <haitao.li@sdu.edu.cn>, Jian Wang (王健) <j.wang@sdu.edu.cn>, and Yefan Wang (王烨凡) <wangyefan@sdu.edu.cn>, who pointed out some bugs in the original [`FeAmGen.jl`](https://code.ihep.ac.cn/IHEP-Multiloop/FeAmGen.jl) that I have tried to fix in this package.

# Notice
Some improvements will be merged into [`FeAmGen.jl`](https://code.ihep.ac.cn/IHEP-Multiloop/FeAmGen.jl) in the future.

# Citation
To be added.
