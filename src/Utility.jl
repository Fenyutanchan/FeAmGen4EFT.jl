# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

const get_ext_dir() = joinpath((dirname ∘ dirname ∘ pathof)(@__MODULE__), "ext")

function check_vertex_U1_conservations(vertex::Vertex)::Bool
    all_U1_charge_name_list = String[]
    for particle ∈ vertex.particle_list
        union!(all_U1_charge_name_list, keys(particle.charges))
    end

    for U1_charge_name ∈ all_U1_charge_list
        U1_charge_sum = 0
        for particle ∈ vertex.particle_list
            U1_charge_sum += particle.charges[U1_charge_name]
        end
        iszero(U1_charge_sum) && return false
    end

    return true
end

function get_Lorentz_symbol_script(expr::Basic)::String
    script_path, scirpt_io = mktemp(; cleanup=false)

    all_free_symbols = free_symbols(expr)
    all_Lorentz_indices = filter(s -> startswith(string(s), "LorMU"), all_free_symbols)
    all_spinorial_indices = filter(s -> startswith(string(s), "spinorial"), all_free_symbols)
    isempty(all_Lorentz_indices) && push!(all_Lorentz_indices, Basic("Lmu"))
    isempty(all_spinorial_indices) && push!(all_spinorial_indices, Basic("sp"))

    script_content = """
    Symbol $(join(all_Lorentz_indices, ", "));
    Symbol $(join(all_spinorial_indices, ", "));

    Set LorentzIndices: $(join(all_Lorentz_indices, ", "));
    Set SpinorialIndices: $(join(all_spinorial_indices, ", "));
    """

    write(scirpt_io, script_content)
    close(scirpt_io)

    return script_path
end

function get_group_symbol_scripts!(
    model::Module, group::Group, expr::Basic,
    remove_lowerscore_dict::Dict{String, String},
    recover_lowerscore_dict::Dict{String, String}
)::Vector{String}
    @assert group ∈ values(model.all_groups)
    group_representations = filter(
        rep -> rep.group == group,
        (collect ∘ values)(model.all_representations)
    )

    script_path_list = [
        get_representation_symbol_script!(
            rep, expr, remove_lowerscore_dict,
            recover_lowerscore_dict
        ) for rep ∈ group_representations
    ]

    return script_path_list
end

function get_mass_symbol_script(model::Module)::String
    script_path, scirpt_io = mktemp(; cleanup=false)

    all_mass_str_list = String[]
    for (_, particle) ∈ model.all_particles
        mass = particle.mass
        iszero(mass) && continue
        union!(all_mass_str_list, [mass.name])
    end

    isempty(all_mass_str_list) && push!(all_mass_str_list, "M")

    script_content = """
    Symbol $(join(all_mass_str_list, ", "));

    Set Mass: $(join(all_mass_str_list, ", "));
    """

    write(scirpt_io, script_content)
    close(scirpt_io)

    return script_path
end

function get_momentum_symbol_script(expr::Basic)::String
    script_path, scirpt_io = mktemp(; cleanup=false)

    all_momentum_list = filter(is_momentum, free_symbols(expr))
    all_loop_momentum_list = filter(is_loop_momentum, all_momentum_list)
    all_external_momentum_list = filter(is_external_momentum, all_momentum_list)
    all_external_massive_momentum_list = filter(is_external_massive_momentum, all_external_momentum_list)
    all_external_massless_momentum_list = filter(is_external_massless_momentum, all_external_momentum_list)

    # isempty(all_momentum_list) && append!(all_momentum_list, Basic("q"), Basic("K"), Basic("k"))
    if isempty(all_loop_momentum_list)
        push!(all_momentum_list, Basic("q"))
        push!(all_loop_momentum_list, Basic("q"))
    end
    # isempty(all_external_momentum_list) && append!(all_external_momentum_list, Basic("K"), Basic("k"))
    if isempty(all_external_massive_momentum_list)
        push!(all_external_massive_momentum_list, Basic("K"))
        push!(all_momentum_list, Basic("K"))
        push!(all_external_momentum_list, Basic("K"))
    end
    if isempty(all_external_massless_momentum_list)
        push!(all_external_massless_momentum_list, Basic("k"))
        push!(all_momentum_list, Basic("k"))
        push!(all_external_momentum_list, Basic("k"))
    end

    script_content = """
    Vector $(join(all_momentum_list, ", "));

    Set Momenta: $(join(all_momentum_list, ", "));
    Set LoopMomenta: $(join(all_loop_momentum_list, ", "));
    Set NonLoopMomenta: $(join(all_external_momentum_list, ", "));
    Set MassiveNonLoopMomenta: $(join(all_external_massive_momentum_list, ", "));
    Set MasslessNonLoopMomenta: $(join(all_external_massless_momentum_list, ", "));
    """

    write(scirpt_io, script_content)
    close(scirpt_io)

    return script_path
end

function get_remove_recover_group_function_name_lowerscore_dict!(
    model::Module, group::Group,
    remove_lowerscore_dict::Dict{String, String},
    recover_lowerscore_dict::Dict{String, String}
)::Nothing
    @assert group ∈ values(model.all_groups)
    for (_, group_function::GroupFunction) ∈ model.all_group_functions
        group_function.group != group && continue
        group_function_name = group_function.function_name
        new_group_function_name = replace(group_function_name, '_' => "")
        remove_lowerscore_dict[group_function_name] = new_group_function_name
        recover_lowerscore_dict[new_group_function_name] = group_function_name
    end
    return nothing
end

function get_representation_symbol_script!(
    rep::Representation, expr::Basic,
    remove_lowerscore_dict::Dict{String, String},
    recover_lowerscore_dict::Dict{String, String}
)::String
    script_path, scirpt_io = mktemp(; cleanup=false)

    all_free_symbol_str_list = map(string, free_symbols(expr))
    all_representation_indices = filter(startswith(rep.name), all_free_symbol_str_list)

    new_all_representation_indices = String[]
    for old_rep_index ∈ all_representation_indices
        new_rep_index = replace(old_rep_index, '_' => "")
        push!(new_all_representation_indices, new_rep_index)
        remove_lowerscore_dict[old_rep_index] = new_rep_index
        recover_lowerscore_dict[new_rep_index] = old_rep_index
    end

    script_content = """
    Symbol $(join(new_all_representation_indices, ", "));

    Set $(replace(rep.name, '_' => ""))Indices: $(join(new_all_representation_indices, ", "));
    """

    write(scirpt_io, script_content)
    close(scirpt_io)

    return script_path
end

is_external_massive_momentum(expr::Union{String, Basic})::Bool = is_sym_index_form(expr, "K")
is_external_massless_momentum(expr::Union{String, Basic})::Bool = is_sym_index_form(expr, "k")
is_external_momentum(expr::Union{String, Basic})::Bool = is_external_massive_momentum(expr) || is_external_massless_momentum(expr)
is_loop_momentum(expr::Union{String, Basic})::Bool = is_sym_index_form(expr, "q")
is_momentum(expr::Union{String, Basic})::Bool = is_external_momentum(expr) || is_loop_momentum(expr)

is_sym_index_form(head::Union{String, Basic})::Function = expr -> is_sym_index_form(expr, head)
function is_sym_index_form(expr::Union{String, Basic}, head::Union{String, Basic})::Bool
    @assert (SymEngine.get_symengine_class ∘ Basic)(expr) == :Symbol
    head_index_regex = Regex("^$head[1-9]+\\d*\$")
    return occursin(head_index_regex, string(expr))
end

function run_FORM(
    form_script_str::String;
    file_name::String="debug",
    multi_thread_flag::Bool=false
)::String
    result_io = IOBuffer()

    try
        (run ∘ pipeline)(
            multi_thread_flag ?
                `$(tform()) -w$(Threads.nthreads()) -q -` :
                `$(form()) -q -`;
            stdin=IOBuffer(form_script_str),
            stdout=result_io
        )
    catch
        write("$file_name.frm", form_script_str)
        @warn "Please check the $(joinpath(pwd(), "$file_name.frm"))!"
        rethrow()
    end

    return (String ∘ take!)(result_io)
end

vectorized_tensor_product(v...) = reduce(vectorized_tensor_product, v)

vectorized_tensor_product(
    v1::Vector{Basic},
    v2::Vector{Basic}
)::Vector{Basic} = (vec ∘ map)(prod, Iterators.product(v1, v2))

vectorized_tensor_product(
    den_list_1::Vector{Vector{Basic}},
    den_list_2::Vector{Vector{Basic}}
)::Vector{Vector{Basic}} = (vec ∘ map)(den_tuple -> vcat(den_tuple...), Iterators.product(den_list_1, den_list_2))
