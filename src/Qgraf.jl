# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

include("QgrafAutoBuild.jl")

const get_Qgraf_output_style_path() = joinpath(get_ext_dir(), "Qgraf", "qgraf_output_style.in")

function generate_Qgraf_model(model::Module)::Nothing
    particle_line_list = String[]
    vertex_line_list = String[]
    
    order_line_str = "[integer v_function :: $(join([order.name for (_, order) ∈ model.all_coupling_orders], ", "))]"

    for particle ∈ values(model.all_particles)
        is_anti(particle) && continue

        particle_name = particle.name
        anti_particle_name = particle.anti_name
        if any(contains(particle_name), particle_line_list)
            @warn "The particle $particle_name has already been added to the particle list! Please check the model!"
            continue
        end
        # any(contains(anti_particle_name), particle_line_list) && continue

        # @assert (particle_name ≠ anti_particle_name) ⊻ particle.self_conjugate "Is the particle $particle self-conjugate? Please check the model!"

        # if contains(particle_name, "anti")
        #     particle_name, anti_particle_name = anti_particle_name, particle_name
        # end

        propagator_type = is_Grassmannian_particle(particle) ? "-" : "+"

        particle_line_str = "[$particle_name, $anti_particle_name, $propagator_type" *
            (is_internal_particle(particle) ? "]" : ", external]")

        push!(particle_line_list, particle_line_str)
    end

    for (_, vertex::Vertex) ∈ model.all_vertices
        if length(vertex.particle_list) < 3
            @warn "We do not support the mixed propagator!\n$vertex"
            continue
        end
        vertex_line = "[" * join([particle.name for particle ∈ vertex.particle_list], ", ") * "; "

        order_str = join([order.name * " = $expnoent" for (order, expnoent) ∈ vertex.coupling.order], ", ")
        for (_, order) ∈ model.all_coupling_orders
            order ∈ keys(vertex.coupling.order) && continue
            order_str *= ", " * order.name * " = 0"
        end

        vertex_line *= order_str * "]"
        push!(vertex_line_list, vertex_line)
    end

    # @show particle_line_list
    # @show vertex_line_list

    rm("qgraf_model.in"; force=true, recursive=true)
    open("qgraf_model.in", "w+") do io
        write(io, order_line_str)
        write(io, "\n\n")
        join(io, particle_line_list, "\n")
        write(io, "\n\n")
        join(io, vertex_line_list, "\n")
        write(io, "\n")
    end

    return nothing
end

function generate_Qgraf_script(
    n_loop::Int,
    in_particle_list::Vector{Particle},
    out_particle_list::Vector{Particle},
    option_list::Vector{String};
    order_list::Dict{CouplingOrder, Int}=Dict{CouplingOrder, Int}(),
    CT_order_list::Dict{CouplingOrder, Int}=Dict{CouplingOrder, Int}()
)::Nothing

    in_particle_line = "in = "
    for (particle_index, particle) ∈ enumerate(in_particle_list)
        ext_mom_head = iszero(particle.mass) ? "k" : "K"
        in_particle_line *= particle.name * "[$ext_mom_head$particle_index], "
    end
    in_particle_line = in_particle_line[begin:end-2] * ";"

    out_particle_line = "out = "
    out_index_offset = length(in_particle_list)
    for (particle_index, particle) ∈ enumerate(out_particle_list)
        ext_mom_head = iszero(particle.mass) ? "k" : "K"
        out_particle_line *= particle.name * "[$(ext_mom_head)$(particle_index+out_index_offset)], "
    end
    out_particle_line = out_particle_line[begin:end-2] * ";"

    option_line = "options = " * join(option_list, ", ") * ";"

    order_line_list = String[]
    for (order, exponent) ∈ order_list
        exponent < 0 && continue
        order_line = "true = vsum[$(order.name), $exponent, $exponent];"
        push!(order_line_list, order_line)
    end
    for (order, exponent) ∈ CT_order_list
        exponent < 1 && continue
        order_line = "true = vsum[$(order.name), 1, $exponent];"
        push!(order_line_list, order_line)
    end

    contents = """
               output = 'qgraf_output.out';
               style = 'qgraf_output_style.in';
               model = 'qgraf_model.in';

               $in_particle_line
               $out_particle_line

               loops = $n_loop;
               loop_momentum = q;

               $option_line


               """
    contents *= join(order_line_list, "\n") * "\n"

    rm("qgraf.dat"; force=true, recursive=true)
    write("qgraf.dat", contents)

    return nothing
end

function run_Qgraf(
    model::Module,
    n_loop::Int,
    in_particle_name_list::Vector{String},
    out_particle_name_list::Vector{String},
    option_list::Vector{String};
    order_list::Dict{String, Int}=Dict{String, Int}(),
    CT_order_list::Dict{String, Int}=Dict{String, Int}()
)::Nothing
    in_particle_list = map(name -> model.all_particles[name], in_particle_name_list)
    out_particle_list = map(name -> model.all_particles[name], out_particle_name_list)
    order_list = Dict{CouplingOrder, Int}(model.all_coupling_orders[K] => V for (K, V) ∈ order_list)
    CT_order_list = Dict{CouplingOrder, Int}(model.all_coupling_orders[K] => V for (K, V) ∈ CT_order_list)
    @assert all(is_external_particle, in_particle_list) "The in-particle list should only contain external particles!"
    @assert all(is_external_particle, out_particle_list) "The out-particle list should only contain external particles!"

    cp(get_Qgraf_output_style_path(), "qgraf_output_style.in"; force=true)
    generate_Qgraf_model(model)
    generate_Qgraf_script(n_loop, in_particle_list, out_particle_list, option_list; order_list=order_list, CT_order_list=CT_order_list)

    rm("qgraf_output.out"; force=true, recursive=true)
    (run ∘ pipeline)(`$(qgraf())`, "qgraf_log.log")
    @assert isfile("qgraf_output.out") "Qgraf is not work correctly. Please check the log file `$(joinpath(pwd(), "qgraf_log.log"))`."

    return nothing
end

function analyze_Qgraf_output(model::Module, Qgraf_output_path::String)::Vector{FeynmanGraph}
    @assert isfile(Qgraf_output_path) "$Qgraf_output_path is not found! Current directory is $(pwd())."

    Feynman_diagrams_Qgraf = try
        YAML.load_file(Qgraf_output_path)
    catch
        @warn "The Qgraf output file is not a valid YAML file! Please check the file $Qgraf_output_path."
        rethrow()
    end

    all_Feynman_diagram_keys = (collect ∘ keys)(Feynman_diagrams_Qgraf)
    sort!(all_Feynman_diagram_keys; by=diagram_key->parse(Int,diagram_key[15:end]))

    # Feynman_diagram_list = Vector{FeynmanGraph}(undef, length(all_Feynman_diagram_keys))
    Feynman_diagram_list = FeynmanGraph[]
    for (_, diagram_key) ∈ enumerate(all_Feynman_diagram_keys)
        # Feynman_diagram = FeynmanGraph()
        CT_valid_flag = true

        graph_data_dict = Feynman_diagrams_Qgraf[diagram_key]
        Feynman_diagram = FeynmanGraph(graph_data_dict["n_loop"])

        # Get internal vertex
        vertex_dict_list = graph_data_dict["vertices"]
        for (vertex_index, vertex_dict) ∈ enumerate(vertex_dict_list)
            particle_list = map(name -> model.all_particles[name], vertex_dict["fields"])
            model_vertex = begin
                # model_vertices= values(model.all_vertices)
                model_vertex_indices = findall(v -> v.particle_list == particle_list, model.all_vertices)
                @assert length(model_vertex_indices) == 1 "There should be only one vertex in the model but got $(length(model_vertex_index))! Please check the model $(model)!"
                model.all_vertices[first(model_vertex_indices)]
            end

            vertex = StandardVertex(vertex_index)
            vertex.expression = Dict("expr_list" => model_vertex.expr_list, "coupling" => model_vertex.coupling.expression)
            vertex.sorted_incident_propagator_indices = vertex_dict["propagator_index_list"] # < 0 for external vertex, > 0 for internal vertex
            @assert add_internal_vertex!(Feynman_diagram, vertex)

            @assert length(Feynman_diagram.internal_vertex_list) == vertex_index "The vertex index is not consistent to the vertex list!"
        end

        external_indices = Int[]

        # Get incoming legs
        for in_leg_dict ∈ graph_data_dict["incoming_propagators"]
            # check consistency to the vertices
            internal_vertex = Feynman_diagram.internal_vertex_list[in_leg_dict["vertex_index"]]
            @assert in_leg_dict["field_index"] ∈ internal_vertex.sorted_incident_propagator_indices "The incoming leg is not consistent to the vertices!"
            push!(external_indices, -in_leg_dict["field_index"])

            in_leg = ExternalLeg(
                internal_vertex,
                EndVertex(-in_leg_dict["in_index"]), # < 0 for incoming
                model.all_particles[in_leg_dict["field"]],
                in_leg_dict["momentum"],
                InwardDirection()
            )
            @assert add_external_leg!(Feynman_diagram, in_leg)
        end

        # Get outgoing legs
        for out_leg_dict ∈ graph_data_dict["outgoing_propagators"]
            # check consistency to the vertices
            internal_vertex = Feynman_diagram.internal_vertex_list[out_leg_dict["vertex_index"]]
            @assert out_leg_dict["field_index"] ∈ internal_vertex.sorted_incident_propagator_indices "The outgoing leg is not consistent to the vertices!"
            push!(external_indices, -out_leg_dict["field_index"])

            out_leg = ExternalLeg(
                internal_vertex,
                EndVertex(out_leg_dict["out_index"]), # > 0 for outgoing
                model.all_particles[out_leg_dict["field"]],
                out_leg_dict["momentum"],
                OutwardDirection()
            )
            @assert add_external_leg!(Feynman_diagram, out_leg)
        end

        Feynman_diagram.external_leg_list = Feynman_diagram.external_leg_list[sortperm(external_indices)]

        # Get propagators
        for propagator_dict ∈ graph_data_dict["remnant_propagators"]
            src_vertex = Feynman_diagram.internal_vertex_list[propagator_dict["birth_index"]]
            dst_vertex = Feynman_diagram.internal_vertex_list[propagator_dict["death_index"]]

            particle = model.all_particles[propagator_dict["field"]]
            propagator = if is_auxiliary_particle(particle)
                !(CT_valid_flag &= (src_vertex == dst_vertex)) && break
                AuxiliaryEdge(
                    src_vertex,
                    dst_vertex
                )
            else
                StandardPropagator(
                    src_vertex,
                    dst_vertex,
                    model.all_particles[propagator_dict["field"]],
                    propagator_dict["momentum"],
                    ForwardDirection()
                )
            end
            @assert add_propagator!(Feynman_diagram, propagator)
        end
        !CT_valid_flag && continue

        Feynman_diagram.signed_symmetry_factor = Basic(graph_data_dict["sign"]) * Basic(graph_data_dict["symmetry_factor"])

        push!(Feynman_diagram_list, Feynman_diagram)
    end

    return Feynman_diagram_list
end
