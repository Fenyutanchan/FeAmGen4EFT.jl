# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

module FeAmGen4EFT

using Artifacts
using FeynmanGraphs
using FORM_jll
using Graphs
using SymEngine
using UniversalFeynmanModel
using YAML

include("Amplitude.jl")
include("GenerateAmplitude.jl")
include("Qgraf.jl")
include("Utility.jl")

export Amplitude, AmplitudeContent

end # module FeAmGen4EFT
