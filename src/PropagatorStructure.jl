# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

function analyze_propagator_structure(
    # model::Module,
    g::FeynmanGraph, propagator::StandardPropagator;
    gauge_fixing_dict::Dict{Particle, Any}=Dict{Particle, Any}() # 1 for Feynman gauge, Inf for unitary gauge
)::Tuple{
    Vector{Basic},  # Lorentz expression numerator list
    Vector{Basic},  # group expression numerator list
    Vector{Basic},  # coefficient list
    Vector{Vector{Basic}}   # denominator list, notice that Rξ gauge will have two denominators!
}
    propagator_id = findfirst(==(propagator), g.propagator_list)
    @assert !isnothing(propagator_id) "Propagator $propagator is not a standard propagator of the Feynman diagram in $(g)!"
    Lorentz_numerator_list = Basic[]
    group_numertor_list = Basic[]
    coefficient_list = Basic[]
    denominator_list = Vector{Basic}[]

    # Only support propagators of spin-0, spin-1/2 and spin-1.
    particle = propagator.particle
    @assert particle.spin ∈ 0:3 "Spin $(Int((particle.spin - 1) / 2)) is not supported!"

    momentum = propagator.momentum
    mass = iszero(particle.mass) ? 0 : particle.mass.name
    width = iszero(particle.width) ? 0 : particle.width.name
    src_id = propagator.src.id
    dst_id = propagator.dst.id
    if particle.spin ∈ [0, 1] # for (Grassmannian) scalar
        # Lorentz numerator
        push!(Lorentz_numerator_list, one(Basic))

        # group numerator
        group_numertor = one(Basic)
        for rep ∈ particle.representations
            group_numertor *= Basic("$(rep.name)_Delta($(rep.name)_p$(propagator_id)v$(dst_id), $(rep.name)_p$(propagator_id)v$(src_id))")
        end
        push!(group_numertor_list, group_numertor)

        # coefficient
        push!(coefficient_list, Basic(im))

        # denominator
        push!(denominator_list, [Basic("Den($momentum, $mass, $width)")])
    elseif particle.spin == 2 # for spin-1/2 fermion
        # Lorentz numerator
        Lorentz_numerator = Basic("GSij($momentum, $mass, spinorialp$(propagator_id)v$(dst_id), spinorialp$(propagator_id)v$(src_id))") # γ^μ p_μ + m
        push!(Lorentz_numerator_list, Lorentz_numerator)

        # group numerator
        group_numertor = one(Basic)
        for rep ∈ particle.representations
            group_numertor *= Basic("$(rep.name)_Delta($(rep.name)_p$(propagator_id)v$(dst_id), $(rep.name)_p$(propagator_id)v$(src_id))")
        end
        push!(group_numertor_list, group_numertor)

        # coefficient
        push!(coefficient_list, Basic(im))

        # denominator
        push!(denominator_list, [Basic("Den($momentum, $mass, $width)")])
    elseif particle.spin == 3 # for spin-1 boson
        # Lorentz numerator
        push!(Lorentz_numerator_list, Basic("MT(LorMUp$(propagator_id)v$(dst_id), LorMUp$(propagator_id)v$(src_id))"))

        # group numerator
        group_numertor = one(Basic)
        for rep ∈ particle.representations
            group_numertor *= Basic("$(rep.name)_Delta($(rep.name)_p$(propagator_id)v$(dst_id), $(rep.name)_p$(propagator_id)v$(src_id))")
        end
        push!(group_numertor_list, group_numertor)

        # coefficient
        push!(coefficient_list, -Basic(im))

        # denominator
        push!(denominator_list, [Basic("Den($momentum, $mass, $width)")])

        if !particle.gauge_boson_flag # do not check the gauge fixing dictionary
            push!(Lorentz_numerator_list,
                Basic("FV($momentum, LorMUp$(propagator_id)v$(src_id))") *
                Basic("FV($momentum, LorMUp$(propagator_id)v$(dst_id))") /
                Basic("$mass^2")
            )
            push!(group_numertor_list, first(group_numertor_list))
            push!(coefficient_list, Basic(im))
            push!(denominator_list, first(denominator_list))
        else # for gauge boson
            ξ = haskey(gauge_fixing_dict, particle) ? gauge_fixing[particle] : 1
            if iszero(1 - ξ) # Feynman gauge
                nothing # to do
            elseif ξ == Inf # unitary gauge, only for massive gauge boson
                @assert (!iszero ∘ Basic)(mass) "Unitary gauge is only for massive gauge boson!"
                push!(Lorentz_numerator_list,
                    Basic("FV($momentum, LorMUp$(propagator_id)v$(src_id))") *
                    Basic("FV($momentum, LorMUp$(propagator_id)v$(dst_id))") /
                    Basic("$mass^2")
                )
                push!(group_numertor_list, first(group_numertor_list))
                push!(coefficient_list, Basic(im))
                push!(denominator_list, first(denominator_list))
            else # General Rξ gauge
                push!(Lorentz_numerator_list,
                    (one(Basic) - Basic(ξ)) *
                    Basic("FV($momentum, LorMUp$(propagator_id)v$(src_id))") *
                    Basic("FV($momentum, LorMUp$(propagator_id)v$(dst_id))")
                )
                push!(group_numertor_list, first(group_numertor_list))
                push!(coefficient_list, Basic(im))
                push!(denominator_list,
                    [
                        Basic("Den($momentum, $mass, $width)"),
                        Basic("Den($momentum, sqrt($ξ) * $mass, $width)")
                    ]
                )
            end
        end
    end

    return Lorentz_numerator_list, group_numertor_list, coefficient_list, denominator_list
end

function analyze_external_leg_structure(
    # model::Module,
    g::FeynmanGraph, l::ExternalLeg
)::Tuple{
    Vector{Basic},  # Lorentz expression numerator list
    Vector{Basic},  # group expression numerator list
    Vector{Basic},  # coefficient list
}
    id = findfirst(==(l), g.external_leg_list)
    @assert !isnothing(id) "External leg $l is not a external leg of the Feynman diagram in $(g)!"
    Lorentz_numerator_list = Basic[]
    group_numertor_list = Basic[one(Basic)]
    coefficient_list = Basic[one(Basic)]

    # Only support fileds of spin-0, spin-1/2 and spin-1.
    particle = l.particle
    @assert particle.spin ∈ 1:3 "Spin $(Int((particle.spin - 1) / 2)) is not supported!"

    momentum = l.momentum
    mass = iszero(particle.mass) ? 0 : particle.mass.name
    # width = iszero(particle.width) ? 0 : particle.width.name

    if particle.spin == 1 # for scalar
        push!(Lorentz_numerator_list, one(Basic))
    elseif particle.spin == 2 # for spin-1/2 fermion
        Lorentz_numerator = if l.momentum_direction == OutwardDirection()
            is_anti(particle) ?
                Basic("SpinorV(spinorialext$id, $momentum, $mass)") :
                Basic("SpinorUBar(spinorialext$id, $momentum, $mass)")
        elseif l.momentum_direction == InwardDirection()
            is_anti(particle) ?
                Basic("SpinorVBar(spinorialext$id, $momentum, $mass)") :
                Basic("SpinorU(spinorialext$id, $momentum, $mass)")
        else
            error("Direction $(l.momentum_direction) is not supported!")
        end
        push!(Lorentz_numerator_list, Lorentz_numerator)
    elseif particle.spin == 3 # for spin-1 boson
        if l.momentum_direction == OutwardDirection()
            push!(Lorentz_numerator_list, Basic("VecEpsConj($momentum, $mass, LorMUext$id)"))
        elseif l.momentum_direction == InwardDirection()
            push!(Lorentz_numerator_list, Basic("VecEps($momentum, $mass, LorMUext$id)"))
        else
            error("Direction $(l.momentum_direction) is not supported!")
        end
    end

    return Lorentz_numerator_list, group_numertor_list, coefficient_list
end
