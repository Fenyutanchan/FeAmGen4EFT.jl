# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

struct AmplitudeContent
    Lorentz_structures::Vector{Basic}
    group_structures::Vector{Basic}
    coefficients::Vector{Basic}
    denominators::Vector{Vector{Basic}}
end

struct Amplitude
    # id::Int
    n_loop::Int
    in_particle_list::Vector{Particle}
    out_particle_list::Vector{Particle}

    amplitude::AmplitudeContent
end

function get_basic_kinematic_relation(amp::Amplitude)::Dict{Basic,Basic}
    result = Dict{Basic, Basic}()
    for (i, p) ∈ (enumerate ∘ vcat)(amp.in_particle_list, amp.out_particle_list)
        k_head = iszero(p.mass) ? "k" : "K"
        k = Basic("$(k_head)$(i)")
        m_sqr = iszero(p.mass) ? zero(Basic) : Basic(p.mass.value)^2
        result[SymFunction("SP")(k)] = m_sqr
        result[SymFunction("SP")(k, k)] = m_sqr
    end

    return result
end
