# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

function analyze_vertex_structure(
    model::Module,
    g::FeynmanGraph, v::InternalVertex
)::Tuple{
    Vector{Basic},  # Lorentz expression list
    Vector{Basic},  # group expression list
    Vector{Basic},   # coefficient list
    Basic           # coupling
}
    @assert v ∈ g.internal_vertex_list "Vertex $v is not an internal vertex of the Feynman diagram $(g)!"
    Lorentz_expr_list = Basic[]
    group_expr_list = Basic[]
    coefficient_list = Basic[]

    for vertex_expr ∈ v.expression["expr_list"]
        push!(Lorentz_expr_list,
            analyze_vertex_Lorentz_structure(vertex_expr.Lorentz_structure.expression, g, v)
        )
        push!(group_expr_list,
            analyze_vertex_group_structure(model, vertex_expr.group_structures, g, v)
        )
        push!(coefficient_list, vertex_expr.coefficient)
    end

    return Lorentz_expr_list, group_expr_list, coefficient_list, Basic(v.expression["coupling"])
end

function analyze_vertex_Lorentz_structure(
    Lorentz_expr_str::String,
    g::FeynmanGraph, v::InternalVertex
)::Basic
    Lorentz_expr_str = replace(Lorentz_expr_str, ' ' => "") # remove all spaces
    Lorentz_expr_str = analyze_vertex_Kronecker_δ(Lorentz_expr_str, v)
    Lorentz_expr_str = analyze_vertex_γμ(Lorentz_expr_str, v)
    Lorentz_expr_str = analyze_vertex_γ5(Lorentz_expr_str, v)
    Lorentz_expr_str = analyze_vertex_σμν(Lorentz_expr_str, v)
    Lorentz_expr_str = analyze_vertex_charge_conj(Lorentz_expr_str, v)
    Lorentz_expr_str = analyze_vertex_gμν(Lorentz_expr_str, v)
    Lorentz_expr_str = analyze_vertex_momentum(Lorentz_expr_str, g, v)
    return Lorentz_expr_str
end

function analyze_vertex_group_structure(
    model::Module,
    group_structures::Dict{Group, String},
    g::FeynmanGraph, v::InternalVertex
)::Basic
    result_list = Basic[]

    for (group, expr_str) ∈ group_structures
        expr_str = replace(expr_str, ' ' => "") # remove all spaces

        sorted_rep_name_list = get_vertex_sorted_rep_name_list(g, v, group)
        for rep_name ∈ sorted_rep_name_list
            rep_name == "singlet" && continue
            @assert haskey(model.all_representations, rep_name) "Got unexpected representation name $(rep_name)!"
        end

        group_name_str_range_list = findall(group.name, expr_str)
        for group_name_str_range ∈ group_name_str_range_list
            end_index = findnext('(', expr_str, last(group_name_str_range)) - 1
            group_function_name = expr_str[first(group_name_str_range):end_index]
            @assert haskey(model.all_group_functions, group_function_name) "Got unexpected group function name $(group_function_name)!"
        end

        selected_group_function_name_list = (collect ∘ keys)(model.all_group_functions)
        filter!(startswith(group.name), selected_group_function_name_list)

        for group_function_name ∈ selected_group_function_name_list
            group_function = model.all_group_functions[group_function_name]
            num_args = length(group_function.arguments)
            while true
                group_function_regex = Regex(group_function_name * "\\((-?\\d+\\,)*-?\\d+\\)")

                group_function_range = findfirst(group_function_regex, expr_str)
                isnothing(group_function_range) && break

                group_function_str = expr_str[group_function_range]
                num_list = begin
                    str_range_list = findall(r"-?\d+", group_function_str)
                    filter!(str_range -> first(str_range) > length(group_function_name), str_range_list)
                    map(
                        str_range -> parse(Int, group_function_str[str_range]),
                        str_range_list
                    )
                end
                @assert num_args == length(num_list) "Got inconsistent number of arguments for $(group_function_str)! Expect $(num_args) but got $(length(num_list))."

                new_group_function_str = group_function_name * "("

                for (arg_index, ii) ∈ enumerate(num_list)
                    if ii < 0
                        rep_name = group_function.arguments[arg_index].name
                        new_group_function_str *= "$(rep_name)_v$(v.id)c$(-ii)," # c for contraction
                    else # ii > 0
                        propagator_index = v.sorted_incident_propagator_indices[ii]
                        arg_rep_name = group_function.arguments[arg_index].name

                        if propagator_index < 0
                            new_group_function_str *= "$(arg_rep_name)_ext$(-propagator_index),"
                        else # propagator_index > 0
                            propagator_particle = g.propagator_list[propagator_index].particle
                            @assert (!isempty ∘ filter)(
                                rep -> arg_rep_name ∈ [rep.name, rep.anti_name],
                                propagator_particle.representations
                            ) "Got unexpected $(propagator_particle) and $(arg_rep_name)!"
                            new_group_function_str *= "$(arg_rep_name)_p$(propagator_index)v$(v.id),"
                        end
                    end
                end

                @assert last(new_group_function_str) == ','
                new_group_function_str = new_group_function_str[begin:end-1] * ")"

                expr_str = replace(expr_str, group_function_str => new_group_function_str)
            end
        end

        push!(result_list, Basic(expr_str))
    end

    return prod(result_list)
end

function analyze_vertex_Kronecker_δ(expr_str::String, v::InternalVertex)::String
    spinorial_δ_regex = r"Identity\(-?\d+\,-?\d+\)"
    Lorentz_δ_regex = r"IdentityL\(-?\d+\,-?\d+\)"

    while true
        spinorial_δ_range = findfirst(spinorial_δ_regex, expr_str)
        isnothing(spinorial_δ_range) && break

        spinorial_δ_str = expr_str[spinorial_δ_range]
        num_range_list = findall(r"-?\d+", spinorial_δ_str)
        @assert length(num_range_list) == 2
        
        new_spinorial_δ_str = "SpinDelta("
        for ii ∈ map(str_range -> parse(Int, spinorial_δ_str[str_range]), num_range_list)
            if ii < 0
                new_spinorial_δ_str *= "spinorialv$(v.id)c$(-ii)," # c for contraction
            else # ii > 0
                propagator_index = v.sorted_incident_propagator_indices[ii]
                if propagator_index < 0
                    new_spinorial_δ_str *= "spinorialext$(-propagator_index),"
                else # propagator_index > 0
                    new_spinorial_δ_str *= "spinorialp$(propagator_index)v$(v.id),"
                end
            end
        end
        @assert last(new_spinorial_δ_str) == ','
        new_spinorial_δ_str = new_spinorial_δ_str[begin:end-1] * ")"

        expr_str = replace(expr_str, spinorial_δ_str => new_spinorial_δ_str)
    end

    while true
        Lorentz_δ_range = findfirst(Lorentz_δ_regex, expr_str)
        isnothing(Lorentz_δ_range) && break

        Lorentz_δ_str = expr_str[Lorentz_δ_range]
        num_range_list = findall(r"-?\d+", Lorentz_δ_str)
        @assert length(num_range_list) == 2

        new_Lorentz_δ_str = "MT("
        for ii ∈ map(str_range -> parse(Int, Lorentz_δ_str[str_range]), num_range_list)
            if ii < 0
                new_Lorentz_δ_str *= "LorMUv$(v.id)c$(-ii)," # c for contraction
            else # ii > 0
                propagator_index = v.sorted_incident_propagator_indices[ii]
                if propagator_index < 0
                    new_Lorentz_δ_str *= "LorMUext$(-propagator_index),"
                else # propagator_index > 0
                    new_Lorentz_δ_str *= "LorMUp$(propagator_index)v$(v.id),"
                end
            end
        end
        @assert last(new_Lorentz_δ_str) == ','
        new_Lorentz_δ_str = new_Lorentz_δ_str[begin:end-1] * ")"

        expr_str = replace(expr_str, Lorentz_δ_str => new_Lorentz_δ_str)
    end

    return expr_str
end

function analyze_vertex_γμ(expr_str::String, v::InternalVertex)::String
    γ_regex = r"Gamma\(-?\d+\,-?\d+\,-?\d+\)"

    while true
        γ_range = findfirst(γ_regex, expr_str)
        isnothing(γ_range) && break

        γ_str = expr_str[γ_range]
        num_range_list = findall(r"-?\d+", γ_str)
        @assert length(num_range_list) == 3

        new_γ_str = "GAij("
        Lor_index = parse(Int, γ_str[popfirst!(num_range_list)])
        if Lor_index < 0
            new_γ_str *= "LorMUv$(v.id)c$(-Lor_index)," # c for contraction
        else # Lor_index > 0
            propagator_index = v.sorted_incident_propagator_indices[Lor_index]
            if propagator_index < 0
                new_γ_str *= "LorMUext$(-propagator_index),"
            else # propagator_index > 0
                new_γ_str *= "LorMUp$(propagator_index)v$(v.id),"
            end
        end
        
        for ii ∈ map(str_range -> parse(Int, γ_str[str_range]), num_range_list)
            if ii < 0
                new_γ_str *= "spinorialv$(v.id)c$(-ii)," # c for contraction
            else # ii > 0
                propagator_index = v.sorted_incident_propagator_indices[ii]
                if propagator_index < 0
                    new_γ_str *= "spinorialext$(-propagator_index),"
                else # propagator_index > 0
                    new_γ_str *= "spinorialp$(propagator_index)v$(v.id),"
                end
            end
        end

        @assert last(new_γ_str) == ','
        new_γ_str = new_γ_str[begin:end-1] * ")"

        expr_str = replace(expr_str, γ_str => new_γ_str)
    end

    return expr_str
end

function analyze_vertex_γ5(expr_str::String, v::InternalVertex)::String
    γ5_regex = r"Gamma5\(-?\d+\,-?\d+\)"
    γ6_regex = r"ProjP\(-?\d+\,-?\d+\)"
    γ7_regex = r"ProjM\(-?\d+\,-?\d+\)"

    while true
        γ5_range = findfirst(γ5_regex, expr_str)
        isnothing(γ5_range) && break

        γ5_str = expr_str[γ5_range]
        num_range_list = findall(r"-?\d+", γ5_str)
        @assert length(num_range_list) == 2

        new_γ5_str = "GA5ij("
        for ii ∈ map(str_range -> parse(Int, γ5_str[str_range]), num_range_list)
            if ii < 0
                new_γ5_str *= "spinorialv$(v.id)c$(-ii)," # c for contraction
            else # ii > 0
                propagator_index = v.sorted_incident_propagator_indices[ii]
                if propagator_index < 0
                    new_γ5_str *= "spinorialext$(-propagator_index),"
                else # propagator_index > 0
                    new_γ5_str *= "spinorialp$(propagator_index)v$(v.id),"
                end
            end
        end

        @assert last(new_γ5_str) == ','
        new_γ5_str = new_γ5_str[begin:end-1] * ")"

        expr_str = replace(expr_str, γ5_str => new_γ5_str)
    end

    while true
        γ6_range = findfirst(γ6_regex, expr_str)
        isnothing(γ6_range) && break

        γ6_str = expr_str[γ6_range]
        num_range_list = findall(r"-?\d+", γ6_str)
        @assert length(num_range_list) == 2

        new_γ6_str = "ProjPij("
        for ii ∈ map(str_range -> parse(Int, γ6_str[str_range]), num_range_list)
            if ii < 0
                new_γ6_str *= "spinorialv$(v.id)c$(-ii)," # c for contraction
            else # ii > 0
                propagator_index = v.sorted_incident_propagator_indices[ii]
                if propagator_index < 0
                    new_γ6_str *= "spinorialext$(-propagator_index),"
                else # propagator_index > 0
                    new_γ6_str *= "spinorialp$(propagator_index)v$(v.id),"
                end
            end
        end

        @assert last(new_γ6_str) == ','
        new_γ6_str = new_γ6_str[begin:end-1] * ")"

        expr_str = replace(expr_str, γ6_str => new_γ6_str)
    end

    while true
        γ7_range = findfirst(γ7_regex, expr_str)
        isnothing(γ7_range) && break

        γ7_str = expr_str[γ7_range]
        num_range_list = findall(r"-?\d+", γ7_str)
        @assert length(num_range_list) == 2

        new_γ7_str = "ProjMij("
        for ii ∈ map(str_range -> parse(Int, γ7_str[str_range]), num_range_list)
            if ii < 0
                new_γ7_str *= "spinorialv$(v.id)c$(-ii)," # c for contraction
            else # ii > 0
                propagator_index = v.sorted_incident_propagator_indices[ii]
                if propagator_index < 0
                    new_γ7_str *= "spinorialext$(-propagator_index),"
                else # propagator_index > 0
                    new_γ7_str *= "spinorialp$(propagator_index)v$(v.id),"
                end
            end
        end

        @assert last(new_γ7_str) == ','
        new_γ7_str = new_γ7_str[begin:end-1] * ")"

        expr_str = replace(expr_str, γ7_str => new_γ7_str)
    end

    return expr_str
end

function analyze_vertex_σμν(expr_str::String, v::InternalVertex)::String
    # σμν_regex = r"Sigma\(-?\d+\,-?\d+\,-?\d+\,-?\d+\)"

    # We do not analyze σμν for now.
    # Because arXiv:2304.09883v1 [hep-ph] gave no details for it.

    return expr_str
end

function analyze_vertex_charge_conj(expr_str::String, v::InternalVertex)::String
    charge_conj_regex = r"C\(-?\d+,-?\d+\)"

    while true
        charge_conj_range = findfirst(charge_conj_regex, expr_str)
        isnothing(charge_conj_range) && break

        charge_conj_str = expr_str[charge_conj_range]
        num_range_list = findall(r"-?\d+", charge_conj_str)
        @assert length(num_range_list) == 2

        new_charge_conj_str = "ChargeConjC("
        for ii ∈ map(str_range -> parse(Int, charge_conj_str[str_range]), num_range_list)
            if ii < 0
                new_charge_conj_str *= "spinorialv$(v.id)c$(-ii)," # c for contraction
            else # ii > 0
                propagator_index = v.sorted_incident_propagator_indices[ii]
                if propagator_index < 0
                    new_charge_conj_str *= "spinorialext$(-propagator_index),"
                else # propagator_index > 0
                    new_charge_conj_str *= "spinorialp$(propagator_index)v$(v.id),"
                end
            end
        end

        @assert last(new_charge_conj_str) == ','
        new_charge_conj_str = new_charge_conj_str[begin:end-1] * ")"

        expr_str = replace(expr_str, charge_conj_str => new_charge_conj_str)
    end

    return expr_str
end

function analyze_vertex_gμν(expr_str::String, v::InternalVertex)::String
    gμν_regex = r"Metric\(-?\d+\,-?\d+\)"

    while true
        gμν_range = findfirst(gμν_regex, expr_str)
        isnothing(gμν_range) && break

        gμν_str = expr_str[gμν_range]
        num_range_list = findall(r"-?\d+", gμν_str)
        @assert length(num_range_list) == 2

        new_gμν_str = "MT("
        for ii ∈ map(str_range -> parse(Int, gμν_str[str_range]), num_range_list)
            if ii < 0
                new_gμν_str *= "LorMUv$(v.id)c$(-ii)," # c for contraction
            else # ii > 0
                propagator_index = v.sorted_incident_propagator_indices[ii]
                if propagator_index < 0
                    new_gμν_str *= "LorMUext$(-propagator_index),"
                else # propagator_index > 0
                    new_gμν_str *= "LorMUp$(propagator_index)v$(v.id),"
                end
            end
        end
        
        @assert last(new_gμν_str) == ','
        new_gμν_str = new_gμν_str[begin:end-1] * ")"

        expr_str = replace(expr_str, gμν_str => new_gμν_str)
    end

    return expr_str
end

function analyze_vertex_momentum(expr_str::String, g::FeynmanGraph, v::InternalVertex)::String
    sorted_momentum_list = get_vertex_sorted_momentum_list(g, v)
    momentum_regex = r"P\(-?\d+\,-?\d+\)"

    while true
        momentum_range = findfirst(momentum_regex, expr_str)
        isnothing(momentum_range) && break

        momentum_str = expr_str[momentum_range]
        num_range_list = findall(r"-?\d+", momentum_str)
        @assert length(num_range_list) == 2

        ii, Lor_ii = map(str_range -> parse(Int, momentum_str[str_range]), num_range_list)

        new_momentum_str = "FV("
        new_momentum_str *= string(sorted_momentum_list[ii]) * ','
        new_momentum_str *= if Lor_ii < 0
                "LorMUv$(v.id)c$(-Lor_ii)" # c for contraction
        else # Lor_ii > 0
            propagator_index = v.sorted_incident_propagator_indices[Lor_ii]
            if propagator_index < 0
                "LorMUext$(-propagator_index)"
            else # propagator_index > 0
                "LorMUp$(propagator_index)v$(v.id)"
            end
        end * ')'
        new_momentum_str = replace(new_momentum_str, ' ' => "") # remove all spaces
        
        expr_str = replace(expr_str, momentum_str => new_momentum_str)
    end

    return expr_str
end

function get_vertex_sorted_momentum_list(g::FeynmanGraph, v::InternalVertex)::Vector{Basic}
    sorted_momentum_list = Vector{Basic}(undef, length(v.sorted_incident_propagator_indices))

    for (ii, index) ∈ enumerate(v.sorted_incident_propagator_indices)
        e = index < 0 ? g.external_leg_list[-index] : g.propagator_list[index]
        if isa(e, AuxiliaryEdge)
            sorted_momentum_list[ii] = zero(Basic)
            continue
        end
        sorted_momentum_list[ii] = if e.momentum_direction == InwardDirection()
            @assert e.src == v "Got unexpected $e and $(v)!"
            Basic(e.momentum)
        elseif e.momentum_direction == OutwardDirection()
            @assert e.src == v "Got unexpected $e and $(v)!"
            -Basic(e.momentum)
        elseif e.momentum_direction == ForwardDirection()
            if e.src == v
                -Basic(e.momentum)
            elseif e.dst == v
                Basic(e.momentum)
            else
                @assert false "Got unexpected $e and $(v)!"
            end
        elseif e.momentum_direction == BackwardDirection()
            if e.src == v
                Basic(e.momentum)
            elseif e.dst == v
                -Basic(e.momentum)
            else
                @assert false "Got unexpected $e and $(v)!"
            end
        else
            @assert false "Got unexpected $(e.momentum_direction)!"
        end
    end

    return sorted_momentum_list
end

function get_vertex_sorted_rep_name_list(g::FeynmanGraph, v::InternalVertex, group::Group)::Vector{String}
    sorted_rep_name_list = Vector{String}(undef, length(v.sorted_incident_propagator_indices))

    for (ii, index) ∈ enumerate(v.sorted_incident_propagator_indices)
        e = index < 0 ? g.external_leg_list[-index] : g.propagator_list[index]
        if isa(e, AuxiliaryEdge)
            sorted_rep_name_list[ii] = "singlet"
            continue
        end
        selected_reps = filter(rep -> rep.group == group, e.particle.representations)

        if isempty(selected_reps)
            sorted_rep_name_list[ii] = "singlet"
        else
            @assert length(selected_reps) == 1
            rep = first(selected_reps)
            sorted_rep_name_list[ii] = is_anti(rep) ? rep.anti_name : rep.name
        end
    end

    return sorted_rep_name_list
end
