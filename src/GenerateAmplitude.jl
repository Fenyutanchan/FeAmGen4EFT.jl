# Copyright (c) 2023 Quan-feng WU <wuquanfeng@ihep.ac.cn>
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

include("VertexStructure.jl")
include("PropagatorStructure.jl")

function generate_amplitude(
    model::Module, g::FeynmanGraph;
    gauge_fixing_dict::Dict{Particle, Any}=Dict{Particle, Any}() # 1 for Feynman gauge, Inf for unitary gauge
)
    g_Lorentz_expr_list = Basic[1]
    g_group_expr_list = Basic[1]
    g_coefficient_list = [g.signed_symmetry_factor]
    g_coupling = Basic(1)

    for v ∈ g.internal_vertex_list
        Lorentz_expr_list, group_expr_list,
            coefficient_list, coupling = analyze_vertex_structure(model, g, v)
        g_Lorentz_expr_list = vectorized_tensor_product(g_Lorentz_expr_list, Lorentz_expr_list)
        g_group_expr_list = vectorized_tensor_product(g_group_expr_list, group_expr_list)
        g_coefficient_list = vectorized_tensor_product(g_coefficient_list, coefficient_list)
        g_coupling *= coupling
    end
    g_denominator_list = [Basic[] for _ ∈ g_Lorentz_expr_list]

    for p ∈ g.propagator_list
        if isa(p, AuxiliaryEdge)
            g_coefficient_list .*= -1
            continue
        end
        Lorentz_numerator_list, group_numertor_list,
            coefficient_list, denominator_list = analyze_propagator_structure(g, p; gauge_fixing_dict=gauge_fixing_dict)
        g_Lorentz_expr_list = vectorized_tensor_product(g_Lorentz_expr_list, Lorentz_numerator_list)
        g_group_expr_list = vectorized_tensor_product(g_group_expr_list, group_numertor_list)
        g_coefficient_list = vectorized_tensor_product(g_coefficient_list, coefficient_list)
        g_denominator_list = vectorized_tensor_product(g_denominator_list, denominator_list)
    end
    
    for l ∈ g.external_leg_list
        Lorentz_expr_list, group_expr_list,
            coefficient_list = analyze_external_leg_structure(g, l)
        g_Lorentz_expr_list = vectorized_tensor_product(g_Lorentz_expr_list, Lorentz_expr_list)
        g_group_expr_list = vectorized_tensor_product(g_group_expr_list, group_expr_list)
        g_coefficient_list = vectorized_tensor_product(g_coefficient_list, coefficient_list)
    end

    g_coupling = subs(g_coupling, Basic("im") => im)

    in_particle_list = map(e -> e.particle, filter(is_inward_external_leg, g.external_leg_list))
    out_particle_list = map(e -> e.particle, filter(is_outward_external_leg, g.external_leg_list))
    amplitude_content = AmplitudeContent(
        contract_Lorentz_structures(model, g_Lorentz_expr_list),
        contract_group_structures(model, g_group_expr_list),
        g_coupling .* g_coefficient_list,
        g_denominator_list
    )
    amplitude = Amplitude(
        g.n_loop,
        in_particle_list,
        out_particle_list,
        amplitude_content
    )

    return amplitude
end

contract_Lorentz_structures(model::Module, exprs::Array)::Array = map(expr -> contract_Lorentz_structures(model, expr), exprs)
function contract_Lorentz_structures(model::Module, expr::Basic)::Basic

    tmp_include_Form_scripts = [
        get_Lorentz_symbol_script(expr),
        get_mass_symbol_script(model),
        get_momentum_symbol_script(expr)
    ]
    include_Form_scripts = vcat(
        tmp_include_Form_scripts,
        map(str -> joinpath(get_ext_dir(), "LorentzFormScripts", str),
            [
                "init_definitions.frm",
                "contract_Lorentz_indices.frm",
                "contract_spinorial_indices.frm"
            ]
        )
    )

    Form_script_str = """
    #-

    Format maple;
    Format nospaces;

    Off Statistics;
    Off FinalStats;

    $(join("#include " .* include_Form_scripts, "\n"))

    Local expr = $expr;
    .sort

    #call ContractSpinorialIndices();
    #call ContractLorentzIndices();
    .sort

    #write "%E", expr
    .sort

    .end
    """

    result_str = try
        run_FORM(Form_script_str)
    catch
        @warn """
        The tmp files are not deleted:
        $(join("    " .* tmp_include_Form_scripts, "\n"))
        Please run `rm -rf $(join(tmp_include_Form_scripts, ' '))` manually for cleaning.
        """
        rethrow()
    end
    rm.(tmp_include_Form_scripts; force=true, recursive=true)

    return Basic(result_str)
end

contract_group_structures(model::Module, exprs::Array)::Array = map(expr -> contract_group_structures(model, expr), exprs)
function contract_group_structures(model::Module, expr::Basic)::Basic
    for (_, group) ∈ model.all_groups
        isempty(group.manipulation_scripts) && continue
        expr = contract_group_structures(model, group, expr)
    end

    return expr
end

function contract_group_structures(model::Module, group::Group, expr::Basic)::Basic
    remove_lowerscore_dict = Dict{String, String}()
    recover_lowerscore_dict = Dict{String, String}()
    get_remove_recover_group_function_name_lowerscore_dict!(
        model, group, remove_lowerscore_dict, recover_lowerscore_dict
    )
    tmp_include_Form_scripts = get_group_symbol_scripts!(
        model, group, expr,
        remove_lowerscore_dict,
        recover_lowerscore_dict
    )
    include_Form_scripts = vcat(
        tmp_include_Form_scripts,
        group.manipulation_scripts
    )

    expr_str = string(expr)
    for (old_str, new_str) ∈ remove_lowerscore_dict
        expr_str = replace(expr_str, old_str => new_str)
    end
    # expr_str = replace(string(expr), remove_lowerscore_dict)

    Form_script_str = """
    #-

    Format maple;
    Format nospaces;

    Off Statistics;
    Off FinalStats;

    $(join("#include " .* include_Form_scripts, "\n"))

    Local expr = $expr_str;
    .sort

    #call ContractGroupIndices();
    .sort

    #write "%E", expr
    .sort

    .end
    """

    result_str = try
        run_FORM(Form_script_str)
    catch
        @warn """
        The tmp files are not deleted:
        $(join("    " .* tmp_include_Form_scripts, "\n"))
        Please run `rm -rf $(join(tmp_include_Form_scripts, ' '))` manually for cleaning.
        """
        rethrow()
    end
    rm.(tmp_include_Form_scripts; force=true, recursive=true)

    for (old_str, new_str) ∈ recover_lowerscore_dict
        result_str = replace(result_str, old_str => new_str)
    end
    # result_str = replace(result_str, recover_lowerscore_dict)

    return Basic(result_str)
end
